<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome Buddy</title>

<!-- Javascript code -->
<script type="text/javascript">

// global variables
var rolesData;
var loadedRoles = false;
var languagesData;
var loadedLanguages = false;
var countriesData;
var loadedCountries = false;
var companiesData;
var loadedCompanies = false;
var personsData;
var loadedPersons = false;

// show movies details
var movieDetailsData;
var loadedMovieDetailsData = false;

var gPrevPersonRoleDropdownValue = null; // for person-role dropdown
var gPrevPersonDropdownValue = null; // for person dropdown
var gPrevCompanyRoleDropdownValue = null; // for company role dropdown
var gPrevCountryDropdownValue = null; // for country dropdown
var booksData;
var loadedBooks = false;

var prevMovieDetailsRequest; // to track if movie details request changed or not

// load books, languages, countries data
function loadNecessaryData()
{
	console.log('welcome buddy!');
	loadRoles();
	loadPersons();
	loadLanguages();
	loadCountries();
	loadBooks();
	loadCompanies();
}

// loading books
function loadBooks()
{
	// if already loaded then return
	if(loadedBooks)
	{
		return;
	}
	
	var thisObj = document.getElementById('bookSelectId');
	// countries data
	var xhrBooks = new XMLHttpRequest();
	// add listener
	xhrBooks.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			booksData = JSON.parse(this.responseText);
			
			// iterate data
			for(var i=0; i<booksData.length; i++)
			{
				var obj = booksData[i];
				thisObj.options[i+2] = new Option(obj.name, obj.bookId); // earlier 2 slots are for choose any and add new
				console.log(obj.bookId+' '+obj.name);
			}
			
		}
		loadedBooks = true;
	};
	xhrBooks.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=books", true);
	xhrBooks.send(null);
}

// loading persons
function loadPersons()
{
	// if already loaded then return
	if(loadedPersons)
	{
		return;
	}
	
	var thisObj = document.getElementById('starcastDivId');
	// countries data
	var xhrPersons = new XMLHttpRequest();
	// add listener
	xhrPersons.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			personsData = JSON.parse(this.responseText);
			
			// iterate data
			for(var i=0; i<personsData.length; i++)
			{
				var obj = personsData[i];
				thisObj.options[i+1] = new Option(obj.firstName+' '+obj.lastName, obj.personId);
				console.log(obj.personId+' '+obj.firstName);
			}
			
		}
		loadedPersons = true;
	};
	xhrPersons.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=persons", true);
	xhrPersons.send(null);
}

// loading roles
function loadRoles()
{
	// if already loaded then return
	if(loadedRoles)
	{
		return;
	}
	
	var thisObj = document.getElementById('starcastDivId');
	// countries data
	var xhrRoles = new XMLHttpRequest();
	// add listener
	xhrRoles.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			rolesData = JSON.parse(this.responseText);
			
			// iterate data
			for(var i=0; i<rolesData.length; i++)
			{
				var obj = rolesData[i];
				thisObj.options[i+1] = new Option(obj.name, obj.roleId);
				console.log(obj.roleId+' '+obj.name);
			}
			
		}
		loadedRoles = true;
	};
	xhrRoles.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=roles", true);
	xhrRoles.send(null);
}

// loading languages
function loadLanguages()
{
	// if already loaded then return
	if(loadedLanguages)
	{
		return;
	}
	
	var thisObj = document.getElementById('languageSelectId');
	// countries data
	var xhrLanguages = new XMLHttpRequest();
	// add listener
	xhrLanguages.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			languagesData = JSON.parse(this.responseText);
			
			// iterate data
			for(var i=0; i<languagesData.length; i++)
			{
				var obj = languagesData[i];
				thisObj.options[i+2] = new Option(obj.name, obj.languageId); // earlier 2 slots are for choose any and add new
				console.log(obj.languageId+' '+obj.name);
			}
			
		}
		loadedLanguages = true;
	};
	xhrLanguages.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=languages", true);
	xhrLanguages.send(null);
}

// loading countries
function loadCountries()
{
	// if already loaded then return
	if(loadedCountries)
	{
		return;
	}
	
	var thisObj = document.getElementById('countrySelectId');
	// countries data
	var xhrCountries = new XMLHttpRequest();
	// add listener
	xhrCountries.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			countriesData = JSON.parse(this.responseText);
			console.log(countriesData);
			
			// iterate data
			for(var i=0; i<countriesData.length; i++)
			{
				var obj = countriesData[i];
				thisObj.options[i+2] = new Option(obj.name, obj.countryId); // earlier 2 slots are for choose any and add new
				console.log(obj.countryId+' '+obj.name);
			}
		}
		loadedCountries = true;
	};
	xhrCountries.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=countries", true);
	xhrCountries.send(null);
}

// loading companies
function loadCompanies()
{
	// if already loaded then return
	if(loadedCompanies)
	{
		return;
	}
	
	var thisObj = document.getElementById('pdcSelectId');
	// countries data
	var xhrCountries = new XMLHttpRequest();
	// add listener
	xhrCountries.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			companiesData = JSON.parse(this.responseText);
			console.log(companiesData);
			
			// iterate data
			for(var i=0; i<companiesData.length; i++)
			{
				var obj = companiesData[i];
				thisObj.options[i+1] = new Option(obj.name, obj.companyId);
				console.log(obj.countryId+' '+obj.name);
			}
			
		}
		loadedCountries = true;
	};
	xhrCountries.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=companies", true);
	xhrCountries.send(null);
}


// loading movie details
function loadMovieDetails(isRequestedWithStarcast)
{
	// if already loaded then return
	// second condition is for handling the case wherein user clicks same button multiple times
	if(loadedMovieDetailsData && (prevMovieDetailsRequest == isRequestedWithStarcast))
	{
		// show movies on webpage
		showExistingMovies(isRequestedWithStarcast, true); // data is assigned above
		// true for clearing the existing HTML to show fresh data
		return;
	}
	
	prevMovieDetailsRequest = isRequestedWithStarcast; // for handling the case wherein user clicks same button multiple times

	// movies details data
	var xhrMovieDetails = new XMLHttpRequest();
	// add listener
	xhrMovieDetails.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			movieDetailsData = JSON.parse(this.responseText);
			console.log(movieDetailsData);
			// show movies on webpage
			showExistingMovies(isRequestedWithStarcast, true); // data is assigned above
			// false to show the data first time and true for clearing the existing HTML to show fresh data
		}
		loadedMovieDetailsData = true;
	};
	xhrMovieDetails.open("GET", "http://localhost:8080/MovieMgr/show?reqtype=moviesdetails&isRequestedWithStarcast="+isRequestedWithStarcast, true);
	xhrMovieDetails.send(null);
}


function showRolesDropdown(a)
{
	
	// if dropdown is already present then no need to create it
	// because each person will have one role only
	if(gPrevPersonRoleDropdownValue == null || gPrevCompanyRoleDropdownValue == null)
	{
		var lStarCastTdId;
		
		if(gPrevPersonRoleDropdownValue == null)
		{
			lStarCastTdId = document.getElementById("starcastDivId");
		}
		else if(gPrevCompanyRoleDropdownValue ==null)
		{
			lStarCastTdId = document.getElementById("pdcDivId");
		}
			
		// create dropdown
		var sel = document.createElement('select');
		
		// set id to this select
		sel.setAttribute('id', a.value+'|'+rid);
		sel.setAttribute('name', 'rolename'+Math.random());
		// TODO
		// iterate data
		for(var i=0; i<rolesData.length; i++)
		{
			var obj = rolesData[i];
			var nm = obj.name;
			var rid = obj.roleId;

			// this value if of the form personname|rolename|personvalue|rolevalue
			var totalRoleValue = a.name+'|'+'rolename'+'|'+a.value+'|'+rid;
			console.log('id '+a.value+' '+a.name+' '+totalRoleValue);
			if(gPrevPersonRoleDropdownValue == null)
			{
				gPrevPersonRoleDropdownValue = a.value+'|'+rid;
				console.log('gPrevPersonRoleDropdownValue-'+gPrevPersonRoleDropdownValue);
			
			}
			else if(gPrevCompanyRoleDropdownValue ==null)
			{
				gPrevCompanyRoleDropdownValue = a.value+'|'+rid;
				console.log('gPrevCompanyRoleDropdownValue-'+gPrevCompanyRoleDropdownValue);
			
			}
			
			// add choose label
			if(i==0)
			{
				var defaultOption = document.createElement('option');
				defaultOption.setAttribute('value', 'defaultOption');
				defaultOption.appendChild(document.createTextNode('choose role name'));
				sel.appendChild(defaultOption);
			}
			
			var opt = document.createElement('option');
			opt.setAttribute("value",totalRoleValue);
			opt.appendChild(document.createTextNode(nm));
			sel.appendChild(opt);
		}
		
		// create label
		var lRoleLabel = document.createElement('label');
		lRoleLabel.appendChild(document.createTextNode('Role:'));
		lStarCastTdId.appendChild(lRoleLabel); // add label to the td
		lStarCastTdId.appendChild(sel); // add dropdown to the td
		lStarCastTdId.appendChild(document.createElement('br')); // add br so that next dropdown will be on new line
	}else
	{
		if(gPrevPersonRoleDropdownValue == null)
		{
			gPrevPersonRoleDropdownValue = a.value;
			console.log('gPrevPersonRoleDropdownValue-'+gPrevPersonRoleDropdownValue);
		}
		else if(gPrevCompanyRoleDropdownValue ==null)
		{
			gPrevCompanyRoleDropdownValue = a.value;
			console.log('gPrevCompanyRoleDropdownValue-'+gPrevCompanyRoleDropdownValue);
		}
			
		return;
	}
}

// show country dropdown
function showCountryDropdown(langCountryDivId, a)
{
	
	// if dropdown is already present then no need to create it
	// because each person will have one role only
	if(gPrevCountryDropdownValue == null)
	{
		var lCountryDivId = document.getElementById(langCountryDivId);
		
		// create dropdown
		var sel = document.createElement('select');
		
		// set id to this select
		sel.setAttribute('id', 'langCountrySelectId');
		sel.setAttribute('name', 'countryname'+Math.random());
			
		// TODO
		// iterate data
		for(var i=0; i<countriesData.length; i++)
		{
			var obj = countriesData[i];
			var nm = obj.name;
			var rid = obj.countryId;
			
			if(gPrevCountryDropdownValue == null)
			{
				gPrevCountryDropdownValue = a.value+'|'+rid;
				console.log('gPrevCountryDropdownValue-'+gPrevCountryDropdownValue);
			
			}
			
			// add choose label
			if(i==0)
			{
				var defaultOption = document.createElement('option');
				defaultOption.setAttribute('value', 'defaultOption');
				defaultOption.appendChild(document.createTextNode('choose country name'));
				sel.appendChild(defaultOption);
			}
			
			var opt = document.createElement('option');
			opt.setAttribute("value",rid);
			opt.appendChild(document.createTextNode(nm));
			sel.appendChild(opt);
		}
		
		// create label
		var lCountryLabel = document.createElement('label');
		lCountryLabel.appendChild(document.createTextNode('Country:'));
		lCountryDivId.appendChild(lCountryLabel); // add label to the td
		lCountryDivId.appendChild(sel); // add dropdown to the td
		lCountryDivId.appendChild(document.createElement('br')); // add br so that next dropdown will be on new line
	}else
	{
		if(gPrevCountryDropdownValue == null)
		{
			gPrevCountryDropdownValue = a.value;
			console.log('gPrevCountryDropdownValue-'+gPrevCountryDropdownValue);
		}
		return;
	}
}

/* This is for adding new cast and role options to the form */
function addNewCast(isAuthor)
{
	// get div tag
	var divStarcast 
	
	if(isAuthor)
	{
		divStarcast = document.getElementById('authorDivId');
	}else
	{
		divStarcast = document.getElementById('starcastDivId');
	}
	gPrevPersonRoleDropdownValue = null; // set this to null to add new role dropdown dynamically
	
	// create person dropdown
	// create dropdown
	var sel = document.createElement('select');
	// set id to this select
		sel.setAttribute('id', 'personnameid');
		sel.setAttribute('name', 'personname'+Math.random());
		
	// if not author then only give option to choose role
	if(!isAuthor)
	{
		sel.setAttribute('onchange','showRolesDropdown(this)'); // add on change event
	}
	// iterate data
	for(var i=0; i<personsData.length; i++)
	{
		var obj = personsData[i];
		var nm = obj.firstName+' '+obj.lastName;
		var pid = obj.personId;
		console.log(pid);
		
		// add choose label
		if(i==0)
		{
			var defaultOption = document.createElement('option');
			defaultOption.setAttribute('value', 'defaultOption');
			defaultOption.appendChild(document.createTextNode('choose person name'));
			sel.appendChild(defaultOption);
		}
		
		var opt = document.createElement('option');
		opt.setAttribute("value",pid);
		opt.appendChild(document.createTextNode(nm));
		sel.appendChild(opt);
	}
	
	// add row to div
	divStarcast.appendChild(sel);
}

/* this is for selecting production and distribution companies*/
function addNewCompany()
{
	// get div tag
	var divPdc = document.getElementById('pdcDivId');
	gPrevCompanyRoleDropdownValue = null; // set this to null to add new role dropdown dynamically
	
	// create company dropdown
	// create dropdown
	var sel = document.createElement('select');
	// set id to this select
		sel.setAttribute('id', 'companyId');
		sel.setAttribute('name', 'companyname'+Math.random());
	sel.setAttribute('onchange','showRolesDropdown(this)'); // add on change event
	
	// iterate data
	for(var i=0; i<companiesData.length; i++)
	{
		var obj = companiesData[i];
		var nm = obj.name;
		var cid = obj.pdId;
		console.log(nm+' '+cid);
		
		// add choose label
		if(i==0)
		{
			var defaultOption = document.createElement('option');
			defaultOption.setAttribute('value', 'defaultOption');
			defaultOption.appendChild(document.createTextNode('choose company name'));
			sel.appendChild(defaultOption);
		}
		
		var opt = document.createElement('option');
		opt.setAttribute("value",cid);
		opt.appendChild(document.createTextNode(nm));
		sel.appendChild(opt);
	}
	
	// add row to div
	divPdc.appendChild(sel);
}

// open the add role form
function openAddRoleForm()
{
	document.getElementById("addRoleFormDivId").style.display = "block";
}

// close the add role form
function closeAddRoleForm()
{
	document.getElementById("addRoleFormDivId").style.display = "none";
}

// close the add person form
function closeAddPersonForm()
{
	document.getElementById("addPersonFormDivId").style.display = "none";
}

// open the add person form
function openAddPersonForm()
{
	document.getElementById("addPersonFormDivId").style.display = "block";
}

// close add book form
function closeAddBookForm()
{
	document.getElementById("addBookFormDivId").style.display = "none";
}

// close add country form
function closeAddCountryForm()
{
	document.getElementById("addCountryFormDivId").style.display = "none";
}

// open the add country form
function openAddCountryForm()
{
	document.getElementById("addCountryFormDivId").style.display = "block";
}

// close add language form
function closeAddLanguageForm()
{
	document.getElementById("addLanguageFormDivId").style.display = "none";
}

// open the add language form
function openAddLanguageForm()
{
	document.getElementById("addLanguageFormDivId").style.display = "block";
}


// close add Company form
function closeAddCompanyForm()
{
	document.getElementById("addCompanyFormDivId").style.display = "none";
}

// open the add Company form
function openAddCompanyForm()
{
	document.getElementById("addCompanyFormDivId").style.display = "block";
}

// open the add person form
function openAddBookForm()
{
	document.getElementById("addBookFormDivId").style.display = "block";
}

function showSuccessMessage(message)
{
	console.log(message);
	var popupId = document.getElementById("mypopup");
	popupId.classList.toggle("show");
}

function saveRoleForm()
{
	// send the form from here
	var roleName = document.getElementById("roleNameInputId").value;

	// parameters to send along with the request	
	var params = 'reqtype=addrole&rolename='+roleName;
	var addRoleResult; // for capturing the result
	
	// send AJAX call to save the role data
	var xhrAddRole = new XMLHttpRequest();
	
	// add listener
	xhrAddRole.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			addRoleResult = JSON.parse(this.responseText);
			console.log(addRoleResult);
			
			// check if successful then load the values from database
			if(addRoleResult.success)
			{
				loadedRoles = false;
				loadRoles();
			}
			
			alert('Saved to database - '+addRoleResult.success);
			//showSuccessMessage(addRoleResult);
		}
	};
	xhrAddRole.open("POST", "http://localhost:8080/MovieMgr/show", true);
	
	// set request headers
	xhrAddRole.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhrAddRole.send(params); // send parameters along with the POST request
	
	// close the form 
	closeAddRoleForm();
}

// saves the country
function saveCountryForm()
{
	// send the form from here
	var countryName = document.getElementById("countryNameInputId").value;
	var shortName = document.getElementById("shortNameCountryInputId").value;
	
	// parameters to send along with the request	
	var params = 'reqtype=addcountry&countryname='+countryName
				+'&shortname='+shortName;
				
	var addCountryResult; // for capturing the result
	
	// send AJAX call to save the country data
	var xhrAddCountry = new XMLHttpRequest();
	
	// add listener
	xhrAddCountry.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			addCountryResult = JSON.parse(this.responseText);
			console.log(addCountryResult);
			//showSuccessMessage(addCountryResult);
			
			// check if successful then load the values from database
			if(addCountryResult.success)
			{
				loadedCountries = false;
				loadCountries();
			}
			
			alert('Saved to database - '+addCountryResult.success);
		}
	};
	xhrAddCountry.open("POST", "http://localhost:8080/MovieMgr/show", true);
	
	// set request headers
	xhrAddCountry.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhrAddCountry.send(params); // send parameters along with the POST request
	
	// close the form 
	closeAddCountryForm();
}


// saves the language
function saveLanguageForm()
{
	// send the form from here
	var languageName = document.getElementById("languageNameInputId").value;
	var shortName = document.getElementById("shortNameLangInputId").value;
	var langCountry = document.getElementById("langCountrySelectId").value;
	
	// parameters to send along with the request	
	var params = 'reqtype=addlanguage&languagename='+languageName
				+'&shortname='+shortName
				+'&languagecountry='+langCountry;
				
	var addLanguageResult; // for capturing the result
	
	// send AJAX call to save the country data
	var xhrAddLanguage = new XMLHttpRequest();
	
	// add listener
	xhrAddLanguage.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			addLanguageResult = JSON.parse(this.responseText);
			console.log(addLanguageResult);
			//showSuccessMessage(addLanguageResult);
			
			// check if successful then load the values from database
			if(addLanguageResult.success)
			{
				loadedLanguages = false;
				loadLanguages();
			}
			
			alert('Saved to database - '+addLanguageResult.success);
		}
	};
	xhrAddLanguage.open("POST", "http://localhost:8080/MovieMgr/show", true);
	
	// set request headers
	xhrAddLanguage.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhrAddLanguage.send(params); // send parameters along with the POST request
	
	// close the form 
	closeAddLanguageForm();
}


// saves the company
function saveCompanyForm()
{
	// send the form from here
	var companyName = document.getElementById("companyNameInputId").value;
	var companyType = document.getElementById("companyTypeInputId").value;
	
	// parameters to send along with the request	
	var params = 'reqtype=addcompany&companyname='+companyName
				+'&companytype='+companyType;
				
	var addCompanyResult; // for capturing the result
	
	// send AJAX call to save the country data
	var xhrAddCompany = new XMLHttpRequest();
	
	// add listener
	xhrAddCompany.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			addCompanyResult = JSON.parse(this.responseText);
			console.log(addCompanyResult);
			//showSuccessMessage(addCompanyResult);
			
			// check if successful then load the values from database
			if(addCompanyResult.success)
			{
				loadedCompanies = false;
				loadCompanies();
			}
			
			alert('Saved to database - '+addCompanyResult.success);
		}
	};
	xhrAddCompany.open("POST", "http://localhost:8080/MovieMgr/show", true);
	
	// set request headers
	xhrAddCompany.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhrAddCompany.send(params); // send parameters along with the POST request
	
	// close the form 
	closeAddCompanyForm();
}

// save new book form
function saveBookForm()
{
	// send the form from here
	var bookName = document.getElementById("bookNameInputId").value;
	var publishDate = document.getElementById("publishDateInputId").value;
	var personName = document.getElementById("personnameid").value;

	// parameters to send along with the request	
	var params = 'reqtype=addbook&bookname='+bookName
				+'&publishdate='+publishDate
				+'&personname='+personName
				;
	var addBookResult; // for capturing the result
	
	// send AJAX call to save the role data
	var xhrAddBook = new XMLHttpRequest();
	
	// add listener
	xhrAddBook.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			addBookResult = JSON.parse(this.responseText);
			console.log(addBookResult);
			//showSuccessMessage(addBookResult);
			
			// check if successful then load the values from database
			if(addBookResult.success)
			{
				loadedBooks = false;
				loadBooks();
			}
			
			alert('Saved to database - '+addBookResult.success);
		}
	};
	xhrAddBook.open("POST", "http://localhost:8080/MovieMgr/show", true);
	
	// set request headers
	xhrAddBook.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhrAddBook.send(params); // send parameters along with the POST request
	
	// close the form 
	closeAddBookForm();
}

// save the person form to the database
function savePersonForm()
{
	// send the form from here
	var firstName = document.getElementById("firstNameInputId").value;
	var middleName = document.getElementById("middleNameInputId").value;
	var lastName = document.getElementById("lastNameInputId").value;
	var nickName = document.getElementById("nickNameInputId").value;
	var gender = document.getElementById("genderInputId").value;
	var dob = document.getElementById("dobInputId").value;
	var wikipage = document.getElementById("wikipageInputId").value;
	
	// parameters to send along with the request	
	var params = 'reqtype=addperson&firstname='+firstName
				+'&middlename='+middleName
				+'&lastname='+lastName
				+'&nickname='+nickName
				+'&gender='+gender
				+'&dob='+dob
				+'&wikipage'+wikipage
				;
				
	var addPersonResult; // for capturing the result
	
	// send AJAX call to save the role data
	var xhrAddPerson = new XMLHttpRequest();
	
	// add listener
	xhrAddPerson.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200)
		{
			addPersonResult = JSON.parse(this.responseText);
			console.log(addPersonResult);
			//showSuccessMessage(addPersonResult);
			
			// check if successful then load the values from database
			if(addPersonResult.success)
			{
				loadedPersons = false;
				loadPersons();
			}
			
			alert('Saved to database - '+addPersonResult.success);
		}
	};
	xhrAddPerson.open("POST", "http://localhost:8080/MovieMgr/show", true);
	
	// set request headers
	xhrAddPerson.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhrAddPerson.send(params); // send parameters along with the POST request
	
	// close the form 
	closeAddPersonForm();
}

/* Fetch and Show movies data 
	Data is already loaded in loadMovieDetails function on click of a button
	and assigned to the movieDetailsData variable
	Here we are only iterating it
*/
function showExistingMovies(isRequestedWithStarcast, clearExistingTable)
{
	var thisObj = document.getElementById('movieDetailsDivId');
	
	// clear existing table first
	if(clearExistingTable)
	{
		thisObj.innerHTML = "";
	}
	
	// create table components now to show this data
	var tableTag = document.createElement('table');
	tableTag.setAttribute('border','1'); // set table border
	
	// iterate data
	for(var i=0; i<movieDetailsData.length; i++)
	{
		var lMovie = movieDetailsData[i];
		console.log(lMovie);
		// here you got all the movie details
		var obj = movieDetailsData[i];
		
		console.log("movie details - "+obj);
		// create row
		var rowTag = document.createElement('tr');
		
		// create tds to show data
		// movie name
		var tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.movieName));
		rowTag.appendChild(tdTag);
		
		// movie type
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.movieType));
		rowTag.appendChild(tdTag);
		
		// show starcast
		if(isRequestedWithStarcast)
		{
			// person name
			tdTag = document.createElement('td');
			tdTag.appendChild(document.createTextNode(lMovie.person));
			rowTag.appendChild(tdTag);
			
			// role name
			tdTag = document.createElement('td');
			tdTag.appendChild(document.createTextNode(lMovie.role));
			rowTag.appendChild(tdTag);
		}
		
		// based on
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.basedOn));
		rowTag.appendChild(tdTag);
		
		// release country
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.country));
		rowTag.appendChild(tdTag);
		
		// language
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.language));
		rowTag.appendChild(tdTag);
		
		// release date
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.releaseDate));
		rowTag.appendChild(tdTag);
		
		// watch date
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.watchDate));
		rowTag.appendChild(tdTag);
		
		// trailer
		tdTag = document.createElement('td');
		tdTag.appendChild(document.createTextNode(lMovie.trailer));
		rowTag.appendChild(tdTag);
		
		tableTag.appendChild(rowTag); // add row to table
	}
	// add table to div now
	thisObj.appendChild(tableTag);
	
}

function chooseActionOnSelect(selectId)
{
	var select = document.getElementById(selectId);
	var selectedValue = select.options[select.selectedIndex].value;
	if(selectedValue == 'addnewcountryfromdropdown')
	{
		// open country form here
		openAddCountryForm();
	}else if(selectedValue == 'addnewlanguagefromdropdown')
	{
		// open language form here
		openAddLanguageForm();
	}else if(selectedValue == 'addnewbookfromdropdown')
	{
		// open book form here
		openAddBookForm();
	}else if(selectedValue == 'addnewpersonfromdropdown')
	{
		// open person form here
		openAddPersonForm();
	}else if(selectedValue == 'addnewrolefromdropdown')
	{
		// open role form here
		openAddRoleForm();
	}else if(selectedValue == 'addnewcompanyfromdropdown')
	{
		// open company form here
		openAddCompanyForm();
	}
}
</script>

<style type="text/css">
/* add role pop-up hidden by default*/
 .addFormPopup
{
	display: none;
	position: fixed;
	bottom: 0;
  right: 15px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

.addPersonFormContainer .cancel, .addRoleFormContainer .cancel, .addBookFormContainer .cancel
{
	background-color: red;
}

.addRoleFormContainer .btn, .addPersonFormContainer .btn, .addBookFormContainer .btn
{
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* add some hover effects to the button */
.addRoleFormContainer .btn:HOVER .openButton:HOVER, .addPersonFormContainer .btn:HOVER .openButton:HOVER, .addBookFormContainer .btn:HOVER .openButton:HOVER
{
	opacity: 1;
}


/* Popup container - can be anything you want */
.successMessagePopup {
  position: relative;
  display: inline-block;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}


/* The actual popup */
.successMessagePopup .popuptext {
  visibility: hidden;
  width: 160px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 8px 0;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: 80px;
}

/* Popup arrow */
.successMessagePopup .popuptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.successMessagePopup .show {
  visibility: visible;
  -webkit-animation: fadeIn 1s;
  animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
  from {opacity: 0;} 
  to {opacity: 1;}
}

@keyframes fadeIn {
  from {opacity: 0;}
  to {opacity:1 ;}
}
</style>
</head>
<body onload="loadNecessaryData()">
<!-- This form is for movie addition -->
<form id="addMovie" method="POST" action="http://localhost:8080/MovieMgr/show">
<input type="hidden" name="reqtype" value="addmovie"/>
<table id="tMovie" border="1">
<thead>
<tr>
<td><h3>Enter Movie Details to Add</h3></td>
<td>
<!-- Add new Person-->
<input id="AddPersonButtonId" type="button" class="openButton" onclick="openAddPersonForm()" value="Add new Person"></input>
<!-- Add new Role-->
<input id="AddRoleButtonId" type="button" class="openButton" onclick="openAddRoleForm()" value="Add new Role"></input>
<!-- Add new book-->
<input id="AddBookButtonId" type="button" class="openButton" onclick="openAddBookForm()" value="Add new Book"></input>
<!-- Add new country-->
<input id="AddCountryButtonId" type="button" class="openButton" onclick="openAddCountryForm()" value="Add new Country"></input>
<!-- Add new language-->
<input id="AddLanguageButtonId" type="button" class="openButton" onclick="openAddLanguageForm()" value="Add new Language"></input>
<!-- Add new language-->
<input id="AddCompanyButtonId" type="button" class="openButton" onclick="openAddCompanyForm()" value="Add new Company"></input>
<br> <!-- added line break -->
<!-- Show Movie Details -->
<input id="showMoviesButtonId" type="button" value="Show Existing Movies" onclick="loadMovieDetails(false)"></input>
<!-- Show Movie Details with star cast-->
<input id="showMoviesButtonId" type="button" value="Show Existing Movies With Starcast" onclick="loadMovieDetails(true)"></input>

</td>
</tr>
</thead>
<!-- Movie Name -->
<tr>
<td><label for="movieNameInputId">Movie Name</label></td>
<td><input id="movieNameInputId" type="text" name="name"></input></td>
</tr>

<!-- Movie Type -->
<tr>
<td><label for="movieTypeSelectId">Movie Type</label></td>
<td>
	<select id="movieTypeSelectId" name="type">
	  <option value="bollywood">Bollywood</option>
	  <option value="hollywood">Hollywood</option>
	  <option value="tollywood">Tollywood</option>
	</select>
</td>
</tr>

<!-- Release date -->
<tr>
<td><label for="releaseDateInputId">Release Date</label></td>
<td><input id="releaseDateInputId" type="date" name="releasedate"></input></td>
</tr>

<!-- Watch date -->
<tr>
<td><label for="watchDateInputId">Watch Date</label></td>
<td><input id="watchDateInputId" type="date" name="watchdate"></input></td>
</tr>

<!-- Runtime -->
<tr>
<td><label for="runtimeInputId">Runtime</label></td>
<td><input id="runtimeInputId" type="text" name="runtime"></input></td>
</tr>

<!-- country -->
<tr>
<td><label for="countrySelectId">Country</label></td>
<td>
<select id="countrySelectId" onchange="chooseActionOnSelect('countrySelectId')" name="country">
<option value="chooseCountryId">choose country</option>
<option value="addnewcountryfromdropdown">Add new</option>
</select>
</td>
</tr>

<!-- language -->
<tr>
<td><label for="languageSelectId">Language</label></td>
<td>
<select id="languageSelectId" onchange="chooseActionOnSelect('languageSelectId')" name="language">
<option value="chooseLanguageId">choose language</option>
<option value="addnewlanguagefromdropdown">Add new</option>
</select>
</td>
</tr>

<!-- books -->
<tr>
<td><label for="bookSelectId">Based on</label></td>
<td>
<select id="bookSelectId" onchange="chooseActionOnSelect('bookSelectId')" name="basedon">
<option value="chooseBookId">choose book</option>
<option value="addnewbookfromdropdown">Add new</option>
</select>
</td>
</tr>

<!-- Trailer link -->
<tr>
<td><label for="trailerInputId">Trailer Link</label></td>
<td><input id="trailerInputId" type="text" name="trailerlink"></input></td>
</tr>

<!-- starcast -->
<tr>
<td><label for="personSelectId">Star Cast</label><input type="button" value="Add new" onclick="addNewCast(false)"> </input></td>
<td  id="starcastTdId">
<div id="starcastDivId"></div>
</td>
</tr>

<!-- production and distribution company -->
<tr>
<td><label for="pdcSelectId">Select Production And <br>Distribution Company</label><input type="button" value="Add new" onclick="addNewCompany()"> </input></td>
<td  id="pdcTdId">
<div id="pdcDivId"></div>
</td>
</tr>


<tr>
<td></td>
<td>
<input id="submitButtonId" type="submit"></input>
<input id="resetButtonId" type="reset"></input>
</td>
</tr>


</table> <!-- movie add table end -->
</form> <!-- movie add form end -->

<!-- Add role popup starts -->
<div class="addFormPopup" id="addRoleFormDivId">
<form class="addRoleFormContainer">
<label for"roleNameInputId">Enter Role Name:</label>
<input id="roleNameInputId" name="rolename" type="text"></input>
<input id="submitRoleButtonId" class="btn" type="button" onclick="saveRoleForm()" value="Save"></input>
<input id="cancelRoleButtonId" class="btn cancel" type="button" onclick="closeAddRoleForm()" value="Cancel"></input>
</form>
</div>
<!-- Add role popup ends -->

<!-- Add Person popup starts -->
<div class="addFormPopup" id="addPersonFormDivId">
<table id="addPersonTableId">
<form class="addPersonFormContainer">
<tr>
<td>
<!-- Person first name -->
<label for"firstNameInputId">Enter First Name:</label>
</td>
<td>
<input id="firstNameInputId" name="firstname" type="text"></input>
</td>

<tr>
<td>
<!-- Person middle name -->
<label for"middleNameInputId">Enter Middle Name:</label>
</td>
<td>
<input id="middleNameInputId" name="middlename" type="text"></input>
</td>

<tr>
<td>
<!-- Person last name -->
<label for"lastNameInputId">Enter Last Name:</label>
</td>
<td>
<input id="lastNameInputId" name="lastname" type="text"></input>
</td>

<tr>
<td>
<!-- Person nick name -->
<label for"nickNameInputId">Enter Nick Name:</label>
</td>
<td>
<input id="nickNameInputId" name="nickname" type="text"></input>
</td>

<tr>
<td>
<!-- Person Gender -->
<label for"genderInputId">Choose Gender:</label>
</td>
<td>
<input id="genderInputId" name="gendername" type="radio" value="male" checked>Male</input>
<input id="genderInputId" name="gendername" type="radio" value="female">Female</input>
<input id="genderInputId" name="gendername" type="radio" value="transgender">Transgender</input>
</td>

<tr>
<td>
<!-- Person date of birth -->
<label for"dobInputId">Choose Date of Birth:</label>
</td>
<td>
<input id="dobInputId" name="dboname" type="date"></input>
</td>

<tr>
<td>
<!-- Person wiki page -->
<label for"wikipageInputId">Enter Wikipage URL:</label>
</td>
<td>
<input id="wikipageInputId" name="wikipagename" type="text"></input>
</td>

<tr>
<td>
</td>
<td>
<input id="submitPersonButtonId" class="btn" type="button" onclick="savePersonForm()" value="Save"></input>
<input id="cancelPersonButtonId" class="btn cancel" type="button" onclick="closeAddPersonForm()" value="Cancel"></input>
</td>
</form>
</table>
</div>
<!-- Add Person popup form ends -->


<!-- Add book popup starts -->
<div class="addFormPopup" id="addBookFormDivId">
<table>
<form class="addBookFormContainer">
<tr>
<td>
<!-- Book name -->
<label for"bookNameInputId">Enter Book Name:</label>
</td>
<td>
<input id="bookNameInputId" name="bookname" type="text"></input>
</td>
<!-- author -->
<tr>
<td><label for="authorSelectId">Select Author</label><input type="button" value="Select Author" onclick="addNewCast(true)"> </input></td>
<td  id="authorTdId">
<div id="authorDivId"></div>
</td>
</tr>

<tr>
<td>
<!-- Publish date -->
<label for"publishDateInputId">Choose Book publish date:</label>
</td>
<td>
<input id="publishDateInputId" name="bookpublishdate" type="date"></input>
</td>
</tr>

<tr>
<td>
</td>
<td>
<input id="submitBookButtonId" class="btn" type="button" onclick="saveBookForm()" value="Save"></input>
<input id="cancelBookButtonId" class="btn cancel" type="button" onclick="closeAddBookForm()" value="Cancel"></input>
</td>
</tr>

</form> <!-- add book form ends -->
</table> <!-- add book table ends -->
</div>
<!-- Add book popup ends -->


<!-- Add country popup starts -->
<div class="addFormPopup" id="addCountryFormDivId">
<table>
<form class="countryRoleFormContainer">
<tr>
<td>
<label for"countryNameInputId">Enter Country Name:</label>
</td>
<td>
<input id="countryNameInputId" name="countryname" type="text"></input><br>
</td>
</tr>
<tr>
<td>
<!-- Short Name -->
<label for"shortNameCountryInputId">Enter Short Name:</label>
</td>
<td>
<input id="shortNameCountryInputId" name="countryshortname" type="text"></input>
</td>
</tr>
<tr>
<td>
</td>
<td>
<input id="submitCountryButtonId" class="btn" type="button" onclick="saveCountryForm()" value="Save"></input>
<input id="cancelCountryButtonId" class="btn cancel" type="button" onclick="closeAddCountryForm()" value="Cancel"></input>
</td>
</tr>
</form>
</table>
</div>
<!-- Add country popup ends -->


<!-- Add language popup starts -->
<div class="addFormPopup" id="addLanguageFormDivId">
<table>
<form class="addLanguageFormContainer">
<tr>
<td>
<!-- Book name -->
<label for"bookNameInputId">Enter Language Name:</label>
</td>
<td>
<input id="languageNameInputId" name="languagename" type="text"></input>
</td>
<!-- author -->
<tr>
<td><label for="langCountryLableId">Select Country in which this language is spoken</label><input type="button" value="Select Country" onclick="showCountryDropdown('langCountryDivId', this)"> </input></td>
<td  id="langCountryTdId">
<div id="langCountryDivId"></div>
</td>
</tr>

<tr>
<td>
<!-- Short Name -->
<label for"shortNameLangInputId">Enter Short Name:</label>
</td>
<td>
<input id="shortNameLangInputId" name="languageshortname" type="text"></input>
</td>
</tr>

<tr>
<td>
</td>
<td>
<input id="submitLanguageButtonId" class="btn" type="button" onclick="saveLanguageForm()" value="Save"></input>
<input id="cancelLanguageButtonId" class="btn cancel" type="button" onclick="closeAddLanguageForm()" value="Cancel"></input>
</td>
</tr>

</form> <!-- add language form ends -->
</table> <!-- add language table ends -->
</div>
<!-- Add language popup ends -->



<!-- Add company popup starts -->
<div class="addFormPopup" id="addCompanyFormDivId">
<table>
<form class="addCompanyFormContainer">
<tr>
<td>
<!-- Book name -->
<label for"companyNameInputId">Enter Company Name:</label>
</td>
<td>
<input id="companyNameInputId" name="companyname" type="text"></input>
</td>

<tr>
<td>
<!-- Short Name -->
<label for"companyTypeInputId">Choose Type:</label>
</td>
<td>
<select id="companyTypeInputId" name="companytype">
	  <option value="0">Production Company</option>
	  <option value="1">Distribution Company</option>
	  <option value="2">Other</option>
	</select>
</td>
</tr>

<tr>
<td>
</td>
<td>
<input id="submitCompanyButtonId" class="btn" type="button" onclick="saveCompanyForm()" value="Save"></input>
<input id="cancelCompanyButtonId" class="btn cancel" type="button" onclick="closeAddCompanyForm()" value="Cancel"></input>
</td>
</tr>

</form> <!-- add company form ends -->
</table> <!-- add company table ends -->
</div>
<!-- Add company popup ends -->

<div class="successMessagePopup">
<span class="popuptext" id="mypopup">Added Successfully</span>
</div>


<div class="movieDetailsTableClass" id="movieDetailsDivId">
</div>
</body>
</html>