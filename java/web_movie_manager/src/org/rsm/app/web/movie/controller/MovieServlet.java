package org.rsm.app.web.movie.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.rsm.app.movie.service.IMovieService;
import org.rsm.app.movie.service.impl.MovieServiceImpl;
import org.rsm.app.movie.util.AppUtility;
import org.rsm.app.web.movie.utilities.IRequestConstants;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author RohitMuneshwar
 * Servlet class for movies
 *
 */
public class MovieServlet extends HttpServlet
{
	IMovieService movieService;
	
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 * All the initialization of the data will be done here
	 */
	@Override
	public void init(ServletConfig cfg) throws ServletException
	{
		//local variables
		String applicationContextFile = "core-applicationcontext.xml";
		System.out.println("Inside init");
		// load bean factory
		try
		{
		//BeanFactory factory = new XmlBeanFactory(new ClassPathResource(applicationContextFile));
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(applicationContextFile);
		movieService = (IMovieService)applicationContext.getBean("movieServiceId");
		if(movieService == null)
		{
			System.out.println("could not instantiate movie service");
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		super.init(cfg);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * Our business logic will be here
	 * This will eventually call doGet or doPost
	 */
	/*@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ServletException, IOException
	{
		System.out.println("inside service");
		super.service(arg0, arg1);
		
	}*/
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * Handles get methods
	 */
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
	{
		System.out.println("Inside doGet");
		List objectList = new ArrayList();
		
		String reqType = req.getParameter("reqtype");
		System.out.println("Request for -"+reqType);
		
		if(reqType != null)
		{
			switch(reqType)
			{
			case IRequestConstants.ROLES:
				objectList = movieService.getRoles();
				break;
			case IRequestConstants.MOVIES:
				objectList = movieService.getMovies();
				break;
			case IRequestConstants.LANGUAGES:
				objectList = movieService.getLanguage();
				break;
			case IRequestConstants.COMPANIES:
				objectList = movieService.getPDCompanies();
				break;
			case IRequestConstants.COUNTRIES:
				objectList = movieService.getCountry();
				break;
			case IRequestConstants.PERSONS:
				objectList = movieService.getPersons();
				break;
			case IRequestConstants.BOOKS:
				objectList = movieService.getBooks();
				break;
			case IRequestConstants.MOVIES_DETAILS:
				boolean isRequestedWithStarcast = Boolean.parseBoolean(req.getParameter(IRequestConstants.IS_REQUESTED_WITH_STARCAST));
				objectList = movieService.getMovieDetails(isRequestedWithStarcast);
				break;
			}
		}else
		{
			System.out.println("Request for is null");
		}
		// show details now
		ServletOutputStream out = resp.getOutputStream();
		//PrintWriter out = resp.getWriter();
		resp.setContentType("application/json"); // set response as json
		/*out.println("<b>Movie Details:</b>");
		List<MovieDetails> movieList = movieService.getMovieDetailsWithStarCast();
		out.println("<table border=1>");
		out.println("<tr>");
		out.println("<td>Movie Name</td>");
		out.println("<td>Movie Type</td>");
		out.println("<td>Person</td>");
		out.println("<td>Role</td>");
		out.println("<td>Release Date</td>");
		out.println("<td>Watch Date</td>");
		out.println("<td>Run Time</td>");
		out.println("<td>Based On</td>");
		out.println("<td>Language</td>");
		out.println("<td>Country</td>");
		out.println("<td>Trailer Link</td>");
		out.println("</tr>");
		for(MovieDetails md : movieList)
		{
			out.println("<tr>");
			out.println("<td>"+md.getMovieName()+"</td>");
			out.println("<td>"+md.getMovieType()+"</td>");
			out.println("<td>"+md.getPerson()+"</td>");
			out.println("<td>"+md.getRole()+"</td>");
			out.println("<td>"+md.getReleaseDate().toString()+"</td>");
			out.println("<td>"+md.getWatchDate().toString()+"</td>");
			out.println("<td>"+md.getRunTime()+"</td>");
			out.println("<td>"+md.getBasedOn()+"</td>");
			out.println("<td>"+md.getLanguage()+"</td>");
			out.println("<td>"+md.getCountry()+"</td>");
			out.println("<td>"+md.getTrailer()+"</td>");
			out.println("</tr>");
		}
		out.println("</table>");*/
		out.print(AppUtility.convertToJson(objectList));
		out.close();
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * Handles post methods
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
	{
		System.out.println("Inside doPost");
		int result = 0;
		Map<String, String> reqParams = new HashMap<>();
		
		String reqType = req.getParameter("reqtype");
		System.out.println("Request for -"+reqType);
		
		System.out.println("Adding request params to map");
		Enumeration<String> paramName = req.getParameterNames();
		
		// extract the parameters from the request
		while(paramName.hasMoreElements())
		{
			String key = paramName.nextElement(); 
			String value = req.getParameter(key);
			System.out.println(key+"-"+value);
			reqParams.put(key, value);
		}
		// get request parameters in map
		
		if(reqType != null)
		{
			switch(reqType)
			{
			case IRequestConstants.ADD_ROLE:
				result = movieService.saveRole(reqParams);
				break;
			case IRequestConstants.ADD_MOVIE:
				result = movieService.saveMovie(reqParams);
				break;
			case IRequestConstants.ADD_LANGUAGE:
				result = movieService.saveLanguage(reqParams);
				break;
			case IRequestConstants.ADD_COMPANY:
				result = movieService.savePDCompany(reqParams);
				break;
			case IRequestConstants.ADD_COUNTRY:
				result = movieService.saveCountry(reqParams);
				break;
			case IRequestConstants.ADD_PERSON:
				result = movieService.savePerson(reqParams);
				break;
			case IRequestConstants.ADD_BOOK:
				result = movieService.saveBook(reqParams);
				break;
			}
		}else
		{
			System.out.println("Request for is null");
		}
		
		// clear the map now
		reqParams.clear();
		
		// show details now
		ServletOutputStream out = resp.getOutputStream();
		resp.setContentType("application/json"); // set response as json
		
		out.print(AppUtility.convertToJson(result));
		out.close();
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#destroy()
	 * uninitialization will be done here
	 */
	@Override
	public void destroy()
	{
		System.out.println("inside Destroy");
		super.destroy();
		
	}
}
