package org.rsm.app.web.movie.utilities;

public interface IRequestConstants
{
	// show
	String ROLES = "roles";
	String MOVIES = "movies";
	String COMPANIES = "companies";
	String LANGUAGES = "languages";
	String COUNTRIES = "countries";
	String PERSONS = "persons";
	String BOOKS = "books";
	String MOVIES_DETAILS = "moviesdetails";
	String IS_REQUESTED_WITH_STARCAST = "isRequestedWithStarcast";
	
	// add
	String ADD_ROLE = "addrole";
	String ADD_MOVIE = "addmovie";
	String ADD_COMPANY = "addcompany";
	String ADD_LANGUAGE = "addlanguage";
	String ADD_COUNTRY = "addcountry";
	String ADD_PERSON = "addperson";
	String ADD_BOOK = "addbook";
}
