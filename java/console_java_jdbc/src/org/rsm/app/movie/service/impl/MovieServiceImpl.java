package org.rsm.app.movie.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.rsm.app.movie.dao.IMovieDao;
import org.rsm.app.movie.dao.cache.Cache;
import org.rsm.app.movie.dao.impl.MovieDaoImpl;
import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.MovieDetails;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;
import org.rsm.app.movie.service.IMovieService;
import org.rsm.app.movie.util.AppUtility;
import org.rsm.app.movie.util.IAppConstants;

/**
 * @author RohitMuneshwar
 * This is implementation class for the IMovieService interface.
 */
public class MovieServiceImpl implements IMovieService 
{
	// data
	IMovieDao movieDao = new MovieDaoImpl();
	
	// methods

	@Override
	public int saveMovie(Movie movie)
	{
		int returnVal = movieDao.insertMovie(movie);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
				// along with this new record
		Cache.stateChangedMovie = true;
		System.out.println("Setting stateChangedMovie variable to true");
		return returnVal;
	}

	@Override
	public int saveBook(Book book)
	{
		int returnVal = movieDao.insertBook(book);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
				// along with this new record
		Cache.stateChangedBook = true;
		System.out.println("Setting stateChangedBook variable to true");
		return returnVal;
	}

	@Override
	public int saveCountry(Country country)
	{
		int returnVal = movieDao.insertCountry(country);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
				// along with this new record
		Cache.stateChangedCountry = true;
		System.out.println("Setting stateChangedCountry variable to true");
		return returnVal;
	}

	@Override
	public int saveLanguage(Language language)
	{
		int returnVal = movieDao.insertLanguage(language);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
				// along with this new record
		Cache.stateChangedLanguage = true;
		System.out.println("Setting stateChangedLanguage variable to true");
		return returnVal;
	}

	@Override
	public int savePerson(Person person)
	{
		int returnVal = movieDao.insertPerson(person);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
				// along with this new record
		Cache.stateChangedPerson = true;
		System.out.println("Setting stateChangedPerson variable to true");
		return returnVal;
	}

	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#saveRole(org.rsm.app.movie.model.Role)
	 * Save the role to the database
	 */
	@Override
	public int saveRole(Role role)
	{
		int returnVal = movieDao.insertRole(role);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
				// along with this new record
		Cache.stateChangedRole = true;
		System.out.println("Setting stateChangedRole variable to true");
		return returnVal;
	}
	
	

	@Override
	public int savePDCompany(Company company)
	{
		int returnVal = movieDao.insertCompany(company);
		// setting this cache variable to true so that once retrieve operation happens it should be fetched fresh data from db
		// along with this new record
		Cache.stateChangedPDCompany = true;
		System.out.println("Setting stateChangedPDCompany variable to true");
		return returnVal;
	}

	@Override
	public List<Movie> getMovies()
	{
		return movieDao.getMovies();
	}

	@Override
	public List<Book> getBooks()
	{
		return movieDao.getBooks();
	}

	@Override
	public List<Country> getCountry()
	{
		return movieDao.getCountries();
	}

	@Override
	public List<Language> getLanguage()
	{
		return movieDao.getLanguages();
	}

	@Override
	public List<Person> getPersons()
	{
		return movieDao.getPersons();
	}

	@Override
	public List<Role> getRoles()
	{
		return movieDao.getRoles();
	}

	@Override
	public List<Company> getPDCompanies()
	{
		return movieDao.getCompanies();
	}

	@Override
	public List<MovieDetails> getMovieDetails(boolean isRequestedWithStarcast)
	{
		if(isRequestedWithStarcast)
		{
			return getMovieDetailsWithStarCast();
		}else
		{
			return movieDao.getMovieDetails();
		}
	}

	@Override
	public List<MovieDetails> getMovieDetailsWithStarCast()
	{
		return movieDao.getMovieDetailsWithStarcast();
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#saveMovie(java.util.Map)
	 * Extract the details from the map and create the Movie object
	 */
	@Override
	public int saveMovie(Map<String, String> map)
	{
		Movie movie = new Movie();
		movie.setName(map.get(IAppConstants.MOVIE_NAME)); // name
		movie.setMovieType(map.get(IAppConstants.MOVIE_TYPE)); // type
		movie.setReleaseDate(AppUtility.convertToTimestamp(map.get(IAppConstants.RELEASE_DATE))); // release date
		movie.setWatchDate(AppUtility.convertToTimestamp(map.get(IAppConstants.WATCH_DATE))); // watch date
		movie.setRunTimeInMinutes(Integer.parseInt(map.get(IAppConstants.RUN_TIME))); // runtime
		movie.setReleaseCountryId(Long.parseLong(map.get(IAppConstants.COUNTRY))); // release country
		movie.setLanguageId(Long.parseLong(map.get(IAppConstants.LANGUAGE))); // language
		movie.setBasedOnId(Long.parseLong(map.get(IAppConstants.BASED_ON))); // book id
		movie.setTrailerLink(map.get(IAppConstants.TRAILER_LINK)); // trailer link
		movie.setMovieId(movie.hashCode()); // set movie id
		
		// iterate now to get the person and their role information
		Person p = null;
		Role r = null;
		Company c = null;
		Set<Person> pSet = new HashSet<Person>();
		Set<Company> cSet = new HashSet<Company>();
		
		for(Map.Entry<String, String> entry : map.entrySet())
		{
			String key = entry.getKey();
			
			/*
			 * If key starts with rollname e.g. rolename0.775242539831219-personname0.682590415941176|rolename|660732016|12
			 * then we have person to role or company to role mapping in the form
			 * personname|rollname|personid|roleid
			 * So we need to extract personid and roleid from this string
			 * */
			if(key.startsWith(IAppConstants.ROLE_NAME))
			{
				// split the string using | seperator
				String[] tempArr = map.get(key).split(IAppConstants.SEP_BAR);
				// 0 - personname/companyname
				// 1 - rolename
				// 2 - personid
				// 3 - roleid
				System.out.println("temp - "+Arrays.asList(tempArr)+" "+tempArr[3]);
				if(tempArr[0].startsWith(IAppConstants.PERSON))
				{
					c = null;
					p = new Person();
					p.setPersonId(Long.parseLong(tempArr[2])); // set person id came from the ui
				}else if(tempArr[0].startsWith(IAppConstants.COMPANY_NAME))
				{
					p = null;
					c = new Company();
					c.setPdId(Long.parseLong(tempArr[2])); // set company id came from the ui
				}
				
				Set<Role> rSet = new HashSet<Role>();
				r = new Role();
				r.setRoleId(Long.valueOf(tempArr[3]));
				rSet.add(r);
				
				if(p!=null)
				{
					p.setRoleIds(rSet);
					pSet.add(p);
				}else if(c!=null)
				{
					cSet.add(c);
				}
			}
		}
		movie.setStarcast(pSet);
		movie.setPdCompanies(cSet);
		System.out.println("pset - "+pSet);
		System.out.println("cset - "+cSet);
		System.out.println(movie);
		
		return saveMovie(movie);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#saveBook(java.util.Map)
	 * Extract the details from the map and create the Book object
	 */
	@Override
	public int saveBook(Map<String, String> map)
	{
		// local variables
		Book book = new Book();
		
		// code
		// set book data
		book.setName(map.get(IAppConstants.BOOK_NAME));
		book.setPublishDate(AppUtility.convertToTimestamp(map.get(IAppConstants.PUBLISH_DATE)));
		book.setBookId(book.hashCode());
		
		for(Map.Entry<String, String> entry : map.entrySet())
		{
			String key = entry.getKey();
			
			if(key.startsWith(IAppConstants.PERSON))
			{
				book.setAuthorId(Long.parseLong(map.get(IAppConstants.PERSON)));
			}
		}
		
		
		return saveBook(book);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#saveCountry(java.util.Map)
	 * Extract the details from the map and create the Country object
	 */
	@Override
	public int saveCountry(Map<String, String> map)
	{
		// local variables
		Country country = new Country();
		int result = 0;
		country.setName(map.get(IAppConstants.COUNTRY_NAME));
		country.setShortName(map.get(IAppConstants.SHORT_NAME));
		country.setCountryId(country.hashCode());
		result = saveCountry(country);
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#saveLanguage(java.util.Map)
	 * Extract the details from the map and create the Language object
	 */
	@Override
	public int saveLanguage(Map<String, String> map)
	{
		// local variables
		Language language = new Language();
		int result = 0;
		language.setName(map.get(IAppConstants.LANUGAGE_NAME));
		language.setShortName(map.get(IAppConstants.SHORT_NAME));
		language.setCountryId(Long.parseLong(map.get(IAppConstants.LANG_COUNTRY_ID)));
		language.setLanguageId(language.hashCode());
		result = saveLanguage(language);
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#savePerson(java.util.Map)
	 * Extract the details from the map and create the Person object
	 */
	@Override
	public int savePerson(Map<String, String> map)
	{
		// local variables
		Person p = new Person();
		
		// code
		// set person date
		p.setFirstName(map.get(IAppConstants.FIRST_NAME));
		p.setMiddleName(map.get(IAppConstants.MIDDLE_NAME));
		p.setLastName(map.get(IAppConstants.LAST_NAME));
		p.setNickName(map.get(IAppConstants.NICK_NAME));
		p.setDateOfBirth(AppUtility.convertToTimestamp(map.get(IAppConstants.DATE_OF_BIRTH)));
		p.setGender(map.get(IAppConstants.GENDER));
		p.setWikiPage(map.get(IAppConstants.WIKI_PAGE));
		p.setPersonId(p.hashCode());
		
		return savePerson(p);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#saveRole(java.util.Map)
	 * Extract the details from the map and create the role object
	 */
	@Override
	public int saveRole(Map<String, String> map)
	{
		// local variables
		Role role = new Role();
		int result = 0;
		if(map.containsKey(IAppConstants.ROLE_NAME))
		{
			role.setName(map.get(IAppConstants.ROLE_NAME));
			result = saveRole(role);
		}
		if(result == 0)
		{
			System.out.println("Could not save role to the database");
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#savePDCompany(java.util.Map)
	 * Extract the details from the map and create the Company object
	 */
	@Override
	public int savePDCompany(Map<String, String> map)
	{
		// local variables
		Company company = new Company();
		company.setName(map.get(IAppConstants.COMPANY_NAME));
		company.setType(Integer.parseInt(map.get(IAppConstants.COMPANY_TYPE)));
		company.setPdId(company.hashCode());
		int result = savePDCompany(company);
		return result;
	}


}
