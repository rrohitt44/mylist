package org.rsm.app.movie.service;

import java.util.List;
import java.util.Map;

import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.MovieDetails;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;

/**
 * @author RohitMuneshwar
 *	This is interface class for the services provided by moive application
 */
public interface IMovieService 
{
	public abstract List<Movie> getMovies(); // show all the available movies
	public abstract List<Book> getBooks(); // show all the available books
	public abstract List<Country> getCountry(); // show all the available countries
	public abstract List<Language> getLanguage(); // show all the available languages
	public abstract List<Person> getPersons(); // show all the available person
	public abstract List<Role> getRoles(); // show all the available roles
	public abstract List<Company> getPDCompanies(); // show all the available production and distribution companies
	public abstract List<MovieDetails> getMovieDetails(boolean isRequestedWithStarcast); // show movies in details
	public abstract List<MovieDetails> getMovieDetailsWithStarCast(); // show movies in details
	
	// save data
	public abstract int saveMovie(Movie movie);
	public abstract int saveBook(Book book);
	public abstract int saveCountry(Country country);
	public abstract int saveLanguage(Language language);
	public abstract int savePerson(Person person);
	public abstract int saveRole(Role role);
	public abstract int savePDCompany(Company company);
	
	public abstract int saveMovie(Map<String, String> map);
	public abstract int saveBook(Map<String, String> map);
	public abstract int saveCountry(Map<String, String> map);
	public abstract int saveLanguage(Map<String, String> map);
	public abstract int savePerson(Map<String, String> map);
	public abstract int saveRole(Map<String, String> map);
	public abstract int savePDCompany(Map<String, String> map);
}
