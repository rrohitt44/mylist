package org.rsm.app.movie.manager;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.MovieDetails;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;
import org.rsm.app.movie.service.IMovieService;
import org.rsm.app.movie.service.impl.MovieServiceImpl;
import org.rsm.app.movie.util.AppUtility;
import org.rsm.app.movie.util.ECompanyType;

/**
 * @author RohitMuneshwar
 *	This class displays the menus and takes the input from the user.
 *	It handles the interaction with the user
 */
public class ViewManager
{
	static IMovieService service = new MovieServiceImpl();
	
	/**
	 * Show main menu to the console
	 */
	public static void displayMainMenu()
	{
		// line 1
		System.out.print("1. Add Movie\t");
		System.out.print("6. Show Movie List\t");
		System.out.println("11. Delete Movie\t");
		
		// line 2
		System.out.print("2. Add Book\t");
		System.out.print("7. Show Book List\t");
		System.out.println("12. Delete Book\t");
		
		// line 3
		System.out.print("3. Add Country\t");
		System.out.print("8. Show Country List\t");
		System.out.println("13. Delete Country\t");
		
		System.out.print("4. Add Language\t");
		System.out.print("9. Show Language List\t");
		System.out.println("14. Delete Language\t");
		
		System.out.print("5. Add Person");
		System.out.print("10. Show Person List");
		System.out.println("15. Delete Person");
		
		
		System.out.print("16. Add Role\t");
		System.out.print("17. Show Roles\t");
		System.out.println("18. Delete Role");
		

		System.out.print("19. Add Company\t");
		System.out.print("20. Show Companies\t");
		System.out.println("21. Delete Company");
		
		
		System.out.println("22. Show Movie Details");
		System.out.println("23. Show Movie Details with Star Cast");
		
		// fourth line
		System.out.println("0. Exit");
		System.out.println("Choose any one of the above:");
	}

	/**
	 * @return
	 * Get the person details from the client
	 */
	public static Person getPerson()
	{
		// local variables
		Scanner sc = new Scanner(System.in);
		Person p = new Person();
		
		// code
		System.out.println("Enter firstName");
		p.setFirstName(sc.next());
		
		System.out.println("Enter middleName");
		p.setMiddleName(sc.next());
		
		System.out.println("Enter lastName");
		p.setLastName(sc.next());
		
		System.out.println("Enter nickName");
		p.setNickName(sc.next());
		
		System.out.println("Enter gender");
		p.setGender(sc.next());
		
		System.out.println("Enter date of birth");
		p.setDateOfBirth(AppUtility.getTimestampFromConsole());
		
		System.out.println("Enter wiki page");
		p.setWikiPage(sc.next());
		
		// set id
		p.setPersonId(p.hashCode());
		
		sc = null;
		return p;
	}

	/**
	 * @return
	 * Get the country details from user
	 */
	public static Country getCountry()
	{
		// local variables
		Scanner sc = new Scanner(System.in);
		Country country = new Country();
		
		// code
		System.out.println("Enter Country name");
		country.setName(AppUtility.getLine());
		
		System.out.println("Enter Country Short code");
		country.setShortName(sc.next());
		
		country.setCountryId(country.hashCode()); // set id
		
		sc = null;
		return country;
	}

	/**
	 * @return
	 * Gets the language information from the console and returns
	 * the language object
	 */
	public static Language getLanguage()
	{
		// local variables
		Scanner sc = new Scanner(System.in);
		Language lang = new Language();
		long pid = 0; // for id
		
		// code
		System.out.println("Enter Language name");
		lang.setName(AppUtility.getLine());
		
		System.out.println("Enter Language Short code");
		lang.setShortName(sc.next());
		
		System.out.println("Choose the country which speaks this language");
		pid = chooseOrAddNewCountry();
		lang.setCountryId(pid);
		
		// set language id
		lang.setLanguageId(lang.hashCode());
		sc = null;
		return lang;
	}

	/**
	 * @return
	 * Get the book details from the user
	 */
	public static Book getBook()
	{
		// local variables
		Scanner sc = new Scanner(System.in); // to take input from console
		Book p = new Book(); // for storing book information
		long pid = 0;
		
		// code
		System.out.println("Enter Book name");
		p.setName(AppUtility.getLine());
		
		pid = chooseOrAddNewPerson(); // choose author
		
		p.setAuthorId(pid);
		
		System.out.println("Enter publish date");
		p.setPublishDate(AppUtility.getTimestampFromConsole());
		
		p.setBookId(p.hashCode());
		sc = null;
		return p;
	}

	/**
	 * @return
	 * Get the movie record from the console
	 * and returns the movie object
	 */
	public static Movie getMovie()
	{
		// local variables
		Scanner sc = new Scanner(System.in); // to take input from console
		Movie p = new Movie(); // for storing book information
		long pid = 0;
		boolean bDone = false;
		
		// code
		System.out.println("Enter Movie name");
		p.setName(AppUtility.getLine()); // movie name
		
		System.out.println("Enter movie type");
		p.setMovieType(sc.next()); // movie type
		
		pid = chooseOrAddNewBook();
		p.setBasedOnId(pid); // book id
		
		// release date
		System.out.println("Enter release date");
		p.setReleaseDate(AppUtility.getTimestampFromConsole());
		
		// watch date
		System.out.println("Enter watch date");
		p.setWatchDate(AppUtility.getTimestampFromConsole());
		
		// runtime
		System.out.println("Enter runtime (in munutes)");
		p.setRunTimeInMinutes(sc.nextInt());
		
		// release country
		System.out.println("Choose the release country");
		pid = chooseOrAddNewCountry();
		p.setReleaseCountryId(pid);
		
		
		// language
		System.out.println("Choose the movie language");
		pid = chooseOrAddNewLanguage();
		p.setLanguageId(pid);
		
		// production company
		boolean selectCompany = false;
		Set<Company> companies = new HashSet<Company>();
		do
		{
		System.out.println("Choose the production company");
		long prdId = chooseOrAddNewPDCompany(ECompanyType.PRODUCTION_COMPANY.getValue());
		
		if(prdId == -2)
		{
			selectCompany = true;
		}else // add to the list of companies
		{
			// create company with above returned Id
			Company company = new Company();
			company.setPdId(prdId);
			company.setType(ECompanyType.PRODUCTION_COMPANY.getValue());
			// add company to the list of companies
			companies.add(company);
		}
		}while(selectCompany != true);
		
		// distribution company
		selectCompany = false;
		do
		{
		System.out.println("Choose the distribution company");
		long prdId = chooseOrAddNewPDCompany(ECompanyType.DISTRIBUTION_COMPANY.getValue());
		
		if(prdId == -2)
		{
			selectCompany = true;
		}else // add to the list of companies
		{
			// create company with above returned Id
			Company company = new Company();
			company.setPdId(prdId);
			company.setType(ECompanyType.DISTRIBUTION_COMPANY.getValue());
			// add company to the list of companies
			companies.add(company);
		}
		}while(selectCompany != true);
		p.setPdCompanies(companies); // set the production and distribution companies now
		
		System.out.println("Enter trailer link");
		p.setTrailerLink(AppUtility.getLine());
		
		// handle star cast
		System.out.println("Select Starcast");
		Set<Person> personIds = new HashSet<Person>();
		
		do
		{
			// this is for the roleIds of the selected persons
			Set<Role> roleIds = new HashSet<Role>();
			
			// choose the cast now
			long selected = chooseOrAddNewPerson();
			System.out.println("enter -2 to finish selection");
			if(selected == -2) // finish
			{
				bDone = true;
			}else // otherwise add to the selection list
			{
				boolean doneRoleSelection = false;
				// choose the role of this cast
				do
				{
				long selectedRole = chooseOrAddNewRole();
				System.out.println("enter -2 to finish selection");
				if(selectedRole == -2)
				{
					doneRoleSelection = true;
				}else
				{
					// create role
					Role role = new Role();
					role.setRoleId(selectedRole);
					roleIds.add(role); // add role to the role list
				}
				}while(doneRoleSelection != true);
				
				// create person
				Person person = new Person();
				person.setPersonId(selected);
				person.setRoleIds(roleIds);
				
				personIds.add(person); // add person to the person list
			}
				
		}while(bDone!=true);
		p.setStarcast(personIds);
		
		// setting primary key
		System.out.println("Setting movie id as "+p.hashCode());
		p.setMovieId(p.hashCode());
		
		sc = null;
		return p;
	}

	/**
	 * @return
	 * Gets role as an input from user
	 */
	public static Role getRole()
	{
		// local variables
		Scanner sc = new Scanner(System.in);
		Role role = new Role();
		
		// code
		System.out.println("Enter Role name");
		role.setName(AppUtility.getLine());
		
		sc = null;
		return role;
	}

	/**
	 * @return
	 */
	/**
	 * @return
	 * Get company from user
	 */
	public static Company getCompany(int type)
	{
		// local variables
		Scanner sc = new Scanner(System.in);
		Company company = new Company();
		
		// code
		System.out.println("Enter company name");
		company.setName(AppUtility.getLine());
		company.setPdId(company.hashCode());
		company.setType(type);
		sc = null;
		return company;
	}
	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#showMovies()
	 * Displays all the movies
	 */
	public static List<Movie> showMovies() 
	{
		System.out.println("inside showMovies()");
		List<Movie> movies = service.getMovies();
		
		// print all movies
		if(movies.size() > 0)
		{
			System.out.println("Available movies are -");
			for(int i=0; i<movies.size(); i++)
			{
				Movie m = movies.get(i);
				System.out.println(i+". "+m.getName());
			}
		}else
		{
			System.out.println("No movies to show");
		}
		System.out.println("exiting from the showMovies()");
		return movies;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#showBooks()
	 * Displays all the books
	 */
	public static List<Book> showBooks() 
	{
		System.out.println("inside showBooks()");
		List<Book> books = service.getBooks();
		
		// print all books
		if(books.size() > 0)
		{
			System.out.println("Available books are -");
			for(int i=0; i<books.size(); i++)
			{
				Book m = books.get(i);
				System.out.println(i+". "+m.getName());
			}
		}else
		{
			System.out.println("No books to show");
		}
		System.out.println("exiting from the showBooks()");
		return books;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#showCountry()
	 * Displays all the countries
	 */
	public static List<Country> showCountry() 
	{
		System.out.println("inside showCountry()");
		List<Country> countries = service.getCountry();
		
		// print all countries
		if(countries.size() > 0)
		{
			System.out.println("Available countries are -");
			for(int i=0; i<countries.size(); i++)
			{
				Country m = countries.get(i);
				System.out.println(i+". "+m.getName());
			}
		}else
		{
			System.out.println("No countries to show");
		}
		System.out.println("exiting from the showCountry()");
		return countries;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#showLanguage()
	 * Displays all the languages
	 */
	public static List<Language> showLanguage() 
	{
		System.out.println("inside showLanguage()");
		List<Language> languages = service.getLanguage();
		
		// print all languages
		if(languages.size() > 0)
		{
			System.out.println("Available languages are -");
			for(int i=0; i<languages.size(); i++)
			{
				Language m = languages.get(i);
				System.out.println(i+". "+m.getName());
			}
		}else
		{
			System.out.println("No languages to show");
		}
		System.out.println("exiting from the showLanguage()");
		return languages;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#showPerson()
	 * Displays all the persons
	 */
	public static List<Person> showPerson() 
	{
		System.out.println("inside showPerson()");
		List<Person> persons = service.getPersons();
		
		// print all persons
		if(persons.size() > 0)
		{
			System.out.println("Available persons are -");
			for(int i=0; i<persons.size(); i++)
			{
				Person m = persons.get(i);
				System.out.println(i+". "+m.getFirstName()+" "+m.getMiddleName()+" "+m.getLastName());
			}
		}else
		{
			System.out.println("No persons to show");
		}
		System.out.println("exiting from the showPerson()");
		return persons;
	}
	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewBook()
	 * Choose existing or add new book
	 */
	public static long chooseOrAddNewBook()
	{
		// local variables
		long bid = 0; // id of the book
		int option = 0; // chosen option
		Book book = null; // book info
		Scanner sc = new Scanner(System.in); // to take input
		
		// code
		System.out.println("movie is based on?");
		System.out.println("Choose from below list");
		System.out.println("-1. Add new");
		List<Book> books = showBooks();
		option = sc.nextInt();
		
		// check if option is valid or not
		if(option <= books.size() && option>=0) // option is available in the list
		{
			System.out.println("Book is chosen from the database");
			book = books.get(option);
			bid = book.getBookId();
			System.out.println("You have choosen - "+book.getName());
		}else if(option == -1)// add new person in the database
		{
			System.out.println("Adding new book in the database");
			book = ViewManager.getBook(); // get book details from the user
			service.saveBook(book); // save to the database
			bid = book.getBookId();
		}else
		{
			bid = -2;
		}
		
		sc = null;
		return bid;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewCountry()
	 * Choose existing or add new country
	 */
	public static long chooseOrAddNewCountry()
	{
		// local variables
		long cid = 0; // id of the country
		int option = 0; // chosen option
		Country country = null; // book info
		Scanner sc = new Scanner(System.in); // to take input
		
		// code
		System.out.println("Choose country from below list");
		System.out.println("-1. Add new");
		List<Country> countries = showCountry();
		option = sc.nextInt();
		
		// check if option is valid or not
		if(option <= countries.size() && option>=0) // option is available in the list
		{
			System.out.println("Country is chosen from the database");
			country = countries.get(option);
			cid = country.getCountryId();
			System.out.println("You have choosen - "+country.getName());
		}else if(option == -1)// add new country in the database
		{
			System.out.println("Adding new country in the database");
			country = ViewManager.getCountry(); // get country details from the user
			service.saveCountry(country); // save to the database
			cid = country.getCountryId();
		}else
		{
			cid = -2;
		}
		
		sc = null;
		return cid;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewLanguage()
	 * Choose existing or add new language
	 */
	public static long chooseOrAddNewLanguage()
	{
		// local variables
		long lid = 0; // id of the langauge
		int option = 0; // chosen option
		Language language = null; // language info
		Scanner sc = new Scanner(System.in); // to take input
		
		// code
		System.out.println("Choose language from below list");
		System.out.println("-1. Add new");
		List<Language> languages = showLanguage();
		option = sc.nextInt();
		
		// check if option is valid or not
		if(option <= languages.size() && option>=0) // option is available in the list
		{
			System.out.println("Country is chosen from the database");
			language = languages.get(option);
			lid = language.getLanguageId();
			System.out.println("You have choosen - "+language.getName());
		}else if(option == -1)// add new lanugage in the database
		{
			System.out.println("Adding new language in the database");
			language = ViewManager.getLanguage(); // get language details from the user
			service.saveLanguage(language); // save to the database
			lid = language.getLanguageId();
		}else 
		{
			lid = -2;
		}
		
		sc = null;
		return lid;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewPerson()
	 * Choose existing or add new person
	 */
	public static long chooseOrAddNewPerson()
	{
		// local variables
		long pid = 0; // id of the person
		int option = 0; // chosen option
		Person person = null; // person itself
		Scanner sc = new Scanner(System.in); // to take input
		
		//code
		System.out.println("Choose person from below list");
		// show list of persons
		System.out.println("-1. Add new");
		List<Person> persons = showPerson();
		option = sc.nextInt(); // get input from user
		
		// check if option is valid or not
		if(option <= persons.size() && option>=0) // option is available in the list
		{
			System.out.println("Person is chosen from the database");
			person = persons.get(option);
			pid = person.getPersonId();
			System.out.println("Choosing "+person.getFirstName()+" as an person");
		}else if(option==-1) // add new person in the database
		{
			System.out.println("Adding new person in the database");
			person = ViewManager.getPerson(); // get person details from the user
			service.savePerson(person); // save to the database
			pid = person.getPersonId();
		}else // want to exit
		{
			pid = -2;
		}
		
		sc = null;
		return pid;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewRole()
	 * choose the existing role or add new role
	 */
	public static long chooseOrAddNewRole()
	{
		System.out.println("inside chooseOrAddNewRole");
		// local variables
		long pid = 0; // id of the role
		int option = 0; // chosen option
		Role role = null; // person itself
		Scanner sc = new Scanner(System.in); // to take input
		
		//code
		System.out.println("Choose role from below list");
		// show list of roles
		System.out.println("-1. Add new");
		List<Role> roles = showRoles();
		option = sc.nextInt(); // get input from user
		
		// check if option is valid or not
		if(option <= roles.size() && option>=0) // option is available in the list
		{
			System.out.println("Author is chosen from the database");
			role = roles.get(option);
			pid = role.getRoleId();
			System.out.println("Choosing "+role.getName()+" as an role");
		}else if(option == -1) // add new person in the database
		{
			System.out.println("Adding new role in the database");
			role = ViewManager.getRole(); // get person details from the user
			service.saveRole(role); // save to the database
			System.out.println("Because of addition of new role in the database we are setting ");
			pid = roles.get(roles.size() - 1).getRoleId() + 1;
		}else // nothing is selected
		{
			pid = -2;
		}
		
		sc = null;
		System.out.println("exiting from chooseOrAddNewRole");
		return pid;
	}
	

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#displayMovieDetails()
	 * Display all the movies in detail
	 */
	public static void displayMovieDetails()
	{
		List<MovieDetails> movies = service.getMovieDetails(false);
		for(MovieDetails md : movies)
		{
			System.out.println(md.getMovieName()+"\t"+md.getMovieType()+"\t"
					+md.getReleaseDate()+"\t"+md.getWatchDate()+"\t"
					+md.getRunTime()+"\t"+md.getBasedOn()+"\t"
					+md.getLanguage()+"\t"+md.getCountry()+"\t"+md.getTrailer());
		}
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#displayMovieDetailsWithStarCast()
	 * Display all the movies in detail along with star cast
	 */
	public static void displayMovieDetailsWithStarCast()
	{
		List<MovieDetails> movies = service.getMovieDetailsWithStarCast();
		for(MovieDetails md : movies)
		{
			System.out.println(md.getMovieName()+"\t"+md.getMovieType()+"\t"
					+md.getPerson()+"\t"+md.getRole()+"\t"
					+md.getReleaseDate()+"\t"+md.getWatchDate()+"\t"
					+md.getRunTime()+"\t"+md.getBasedOn()+"\t"
					+md.getLanguage()+"\t"+md.getCountry()+"\t"+md.getTrailer());
		}
	}
	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewMovie()
	 * Choose existing or add new movie
	 */
	public static long chooseOrAddNewMovie()
	{
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#showRoles()
	 * Show existing roles
	 */
	public static List<Role> showRoles()
	{
		System.out.println("inside showRoles()");
		List<Role> roles = service.getRoles();
		
		// print all countries
		if(roles.size() > 0)
		{
			System.out.println("Available roles are -");
			for(int i=0; i<roles.size(); i++)
			{
				Role m = roles.get(i);
				System.out.println(i+". "+m.getName());
			}
		}else
		{
			System.out.println("No Roles to show");
		}
		System.out.println("exiting from the showRoles()");
		return roles;
	}


	/* (non-Javadoc)
	 * @see org.rsm.app.movie.service.IMovieService#chooseOrAddNewPDCompany()
	 * choose or ad new company
	 */
	public static long chooseOrAddNewPDCompany(int type)
	{
		System.out.println("inside chooseOrAddNewPDCompany");
		// local variables
		long pid = 0; // id of the company
		int option = 0; // chosen option
		Company company = null; // company itself
		Scanner sc = new Scanner(System.in); // to take input
		
		//code
		System.out.println("Choose company from below list");
		// show list of roles
		System.out.println("-1. Add new");
		List<Company> companies = showPDCompanies();
		option = sc.nextInt(); // get input from user
		
		// check if option is valid or not
		if(option <= companies.size() && option>=0) // option is available in the list
		{
			System.out.println("Company is chosen from the database");
			company = companies.get(option);
			pid = company.getPdId();
			System.out.println("Choosing "+company.getName()+" as a company");
		}else if(option == -1)// add new person in the database
		{
			System.out.println("Adding new company in the database");
			company = ViewManager.getCompany(type); // get company details from the user
			service.savePDCompany(company); // save to the database
			System.out.println("Because of addition of new company in the database we are setting ");
			pid = company.hashCode();
		}else  // do nothing
		{
			pid = -2;
		}
		
		sc = null;
		System.out.println("exiting from chooseOrAddNewCompany");
		return pid;
	}

	public static List<Company> showPDCompanies()
	{
		System.out.println("inside showPDCompanies()");
		List<Company> companies = service.getPDCompanies();
		
		// print all countries
		if(companies.size() > 0)
		{
			System.out.println("Available companies are -");
			for(int i=0; i<companies.size(); i++)
			{
				Company m = companies.get(i);
				System.out.println(i+". "+m.getName());
			}
		}else
		{
			System.out.println("No Companies to show");
		}
		System.out.println("exiting from the showPDCompanies()");
		return companies;
	}
}
