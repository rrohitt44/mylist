package org.rsm.app.movie.manager;

import java.util.Scanner;

import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;
import org.rsm.app.movie.service.IMovieService;
import org.rsm.app.movie.service.impl.MovieServiceImpl;
import org.rsm.app.movie.util.ECompanyType;

/**
 * @author RohitMuneshwar
 * This class is main driver class for access the database.
 *
 */
public class Driver 
{
	/**
	 * @param args
	 * Starting point for the driver class
	 */
	public static void main(String[] args) 
	{
		System.out.println("Welcome to the application");
		// local variables
		Scanner sc = new Scanner(System.in);
		IMovieService service = new MovieServiceImpl();
		int option = 0;
		int dbPersisted = 0;

		// code
		do
		{
		ViewManager.displayMainMenu(); // display main menu
		option = sc.nextInt();
		
		switch(option)
		{
		case 1: // add movie
			Movie movie = ViewManager.getMovie();
			dbPersisted = service.saveMovie(movie);
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the movie");
			}
			break;
			
		case 2: // add book
			Book book = ViewManager.getBook();
			dbPersisted = service.saveBook(book);
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the book");
			}
			break;
			
		case 3: // add country
			Country country = ViewManager.getCountry();
			dbPersisted = service.saveCountry(country); // save country to database
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the country");
			}
			break;
			
		case 4: // add language
			Language lang = ViewManager.getLanguage();
			dbPersisted = service.saveLanguage(lang);
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the language");
			}
			break;
			
		case 5: // add person
			Person person = ViewManager.getPerson();
			dbPersisted = service.savePerson(person);
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the person");
			}
			break;
		case 6: // show movie
			ViewManager.showMovies();
			break;
			
		case 7: // show book
			ViewManager.showBooks();
			break;
			
		case 8: // show country
			ViewManager.showCountry();
			break;
			
		case 9: // show language
			ViewManager.showLanguage();
			break;
			
		case 10: // show Person list
			ViewManager.showPerson();
			break;
			
		case 11: // delete movie
			// TODO
			System.out.println("Logic is yet to be implemented");
			break;
			
		case 12: // delete book
			// TODO
			System.out.println("Logic is yet to be implemented");
			break;
			
		case 13: // delete country
			// TODO
			System.out.println("Logic is yet to be implemented");
			break;
			
		case 14: // delete language
			// TODO
			System.out.println("Logic is yet to be implemented");
			break;
			
		case 15: // delete person
			// TODO
			System.out.println("Logic is yet to be implemented");
			break;
			
		case 16: // add role
			Role role = ViewManager.getRole();
			dbPersisted = service.saveRole(role);
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the role");
			}
			break;
			
		case 17: // show roles
			ViewManager.showRoles();
			break;
			
		case 18: // delete role
			// TODO
			System.out.println("Logic is yet to be implemented");
			break;
			
		case 19: // add company
			Company company = ViewManager.getCompany(ECompanyType.PRODUCTION_COMPANY.getValue());
			dbPersisted = service.savePDCompany(company);
			if(dbPersisted!=1)
			{
				System.out.println("Error occured while persisting the company");
			}
			break;
			
		case 20: // show companies
			ViewManager.showPDCompanies();
			break;
			
		case 21: // delete company
			// TODO
			System.out.println("service not implemented yet.");
			break;
			
		case 22: // show movie details
			ViewManager.displayMovieDetails();
			break;
		case 23: // show movie details with starcast
			ViewManager.displayMovieDetailsWithStarCast();;
			break;
		case 0:
			System.out.println("Exiting");
			break;
		default:
				System.out.println("Choose correct option");
				break;
		}
		
		if(option!=0)
		{
			System.out.println("Press any key to show main menu");
			sc.next();
		}
		}while(option != 0);
		System.out.println("Thanks for visiting");
	}
}
