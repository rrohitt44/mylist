package org.rsm.app.movie.model;

import java.util.Set;

/**
 * @author RohitMuneshwar
 * This is person pojo class
 */
public class Person 
{
	// data
	private long personId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String nickName;
	private String gender;
	private java.sql.Timestamp dateOfBirth;
	private String wikiPage;
	private Set<Role> roleIds;
	
	// getter-setters
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public java.sql.Timestamp getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(java.sql.Timestamp dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getWikiPage() {
		return wikiPage;
	}
	public void setWikiPage(String wikiPage) {
		this.wikiPage = wikiPage;
	}
	
	public Set<Role> getRoleIds()
	{
		return roleIds;
	}
	public void setRoleIds(Set<Role> roleIds)
	{
		this.roleIds = roleIds;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((roleIds == null) ? 0 : roleIds.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (dateOfBirth == null)
		{
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (firstName == null)
		{
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null)
		{
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null)
		{
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (roleIds == null)
		{
			if (other.roleIds != null)
				return false;
		} else if (!roleIds.equals(other.roleIds))
			return false;
		return true;
	}
	@Override
	public String toString()
	{
		return "{ \"personId\":\"" + personId + "\", \"firstName\":\"" + firstName
				+ "\", \"middleName\":\"" + middleName + "\", \"lastName\":\"" + lastName
				+ "\", \"nickName\":\"" + nickName + "\", \"gender\":\"" + gender
				+ "\", \"dateOfBirth\":\"" + dateOfBirth + "\", \"wikiPage\":\"" + wikiPage
				+ "\", \"roleIds\":\"" + roleIds + "\"}";
	}
	
	
}
