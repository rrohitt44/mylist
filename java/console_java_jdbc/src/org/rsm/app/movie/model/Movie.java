package org.rsm.app.movie.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

/**
 * @author RohitMuneshwar
 * This class represent the Movie details.
 */
public class Movie 
{
	// data
	long movieId; // primary key
	String name; // name of the movie
	String movieType; // type of the movie
	long basedOnId; // id from Book
	Timestamp releaseDate; // release date of the movie
	Timestamp watchDate; // watch date of the movie
	int runTimeInMinutes; // movie run time
	long releaseCountryId; // id in country
	long languageId; // id in language
	String trailerLink; // movie trailer link
	
	// many to many relationships
	// one movie have multiple starcast
	Set<Person> starcast;
	Set<Company> pdCompanies;
	
	// getter-setters
	public long getMovieId() {
		return movieId;
	}
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMovieType() {
		return movieType;
	}
	public void setMovieType(String movieType) {
		this.movieType = movieType;
	}
	public long getBasedOnId() {
		return basedOnId;
	}
	public void setBasedOnId(long basedOnId) {
		this.basedOnId = basedOnId;
	}
	public Timestamp getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Timestamp releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Timestamp getWatchDate() {
		return watchDate;
	}
	public void setWatchDate(Timestamp watchDate) {
		this.watchDate = watchDate;
	}
	public int getRunTimeInMinutes() {
		return runTimeInMinutes;
	}
	public void setRunTimeInMinutes(int runTimeInMinutes) {
		this.runTimeInMinutes = runTimeInMinutes;
	}
	public long getReleaseCountryId() {
		return releaseCountryId;
	}
	public void setReleaseCountryId(long releaseCountryId) {
		this.releaseCountryId = releaseCountryId;
	}
	public long getLanguageId() {
		return languageId;
	}
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}
	public String getTrailerLink() {
		return trailerLink;
	}
	public void setTrailerLink(String trailerLink) {
		this.trailerLink = trailerLink;
	}
	public Set<Person> getStarcast()
	{
		return starcast;
	}
	public void setStarcast(Set<Person> starcast)
	{
		this.starcast = starcast;
	}
	
	public Set<Company> getPdCompanies()
	{
		return pdCompanies;
	}
	public void setPdCompanies(Set<Company> pdCompanies)
	{
		this.pdCompanies = pdCompanies;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((movieType == null) ? 0 : movieType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((releaseDate == null) ? 0 : releaseDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (movieType == null)
		{
			if (other.movieType != null)
				return false;
		} else if (!movieType.equals(other.movieType))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (releaseDate == null)
		{
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		return true;
	}
	@Override
	public String toString()
	{
		return "{ \"movieId\":\"" + movieId + "\", \"name\":\"" + name + "\", \"movieType\":\""
				+ movieType + "\", \"basedOnId\":\"" + basedOnId + "\", \"releaseDate\":\""
				+ releaseDate + "\", \"watchDate\":\"" + watchDate
				+ "\", \"runTimeInMinutes\":\"" + runTimeInMinutes
				+ "\", \"releaseCountryId\":\"" + releaseCountryId + "\", \"languageId\":\""
				+ languageId + "\", \"trailerLink\":\"" + trailerLink + "\", \"starcast\":\""
				+ starcast + "\", \"pdCompanies\":\"" + pdCompanies + "\"}";
	}
	
	
}
