package org.rsm.app.movie.model;

/**
 * @author RohitMuneshwar
 * This is a pojo class for production and distribution companies
 */
public class Company
{
	// data
	long pdId;
	String name;
	int type; // 0-production company and 1-distribution company
	// getter-setters
	public long getPdId()
	{
		return pdId;
	}
	public void setPdId(long pdId)
	{
		this.pdId = pdId;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	
	
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int) (pdId ^ (pdId >>> 32));
		result = prime * result + type;
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pdId != other.pdId)
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	@Override
	public String toString()
	{
		return "{ \"pdId\":\"" + pdId + "\", \"name\":\"" + name + "\", \"type\":\"" + type
				+ "\"}";
	}
}
