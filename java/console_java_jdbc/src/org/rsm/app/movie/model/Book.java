package org.rsm.app.movie.model;

/**
 * @author RohitMuneshwar
 * This is a pojo class for "based-on" movie.
 */
public class Book 
{
	// data
	long bookId;
	String name;
	long authorId;
	java.sql.Timestamp publishDate;
	
	// getter-setters
	public long getBookId() {
		return bookId;
	}
	public void setBookId(long bookId) {
		this.bookId = bookId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}
	public java.sql.Timestamp getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(java.sql.Timestamp publishDate) {
		this.publishDate = publishDate;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((publishDate == null) ? 0 : publishDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (publishDate == null)
		{
			if (other.publishDate != null)
				return false;
		} else if (!publishDate.equals(other.publishDate))
			return false;
		return true;
	}
	@Override
	public String toString()
	{
		return "{ \"bookId\" :\"" + bookId + "\", \"name\" :\"" + name + "\", \"authorId\":\""
				+ authorId + "\", \"publishDate\":\"" + publishDate + "\"}";
	}
	
}
