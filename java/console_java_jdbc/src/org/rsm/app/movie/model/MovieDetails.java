package org.rsm.app.movie.model;

/**
 * @author RohitMuneshwar
 * This class represent the movie with details
 */
public class MovieDetails
{
	// data
	private String movieName;
	private String movieType;
	private String person;
	private String role;
	private java.sql.Date releaseDate;
	private java.sql.Date watchDate;
	private int runTime;
	private String basedOn;
	private String language;
	private String country;
	private String trailer;
	public String getMovieName()
	{
		return movieName;
	}
	public void setMovieName(String movieName)
	{
		this.movieName = movieName;
	}
	public String getMovieType()
	{
		return movieType;
	}
	public void setMovieType(String movieType)
	{
		this.movieType = movieType;
	}
	public String getPerson()
	{
		return person;
	}
	public void setPerson(String person)
	{
		this.person = person;
	}
	public String getRole()
	{
		return role;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	public java.sql.Date getReleaseDate()
	{
		return releaseDate;
	}
	public void setReleaseDate(java.sql.Date releaseDate)
	{
		this.releaseDate = releaseDate;
	}
	public java.sql.Date getWatchDate()
	{
		return watchDate;
	}
	public void setWatchDate(java.sql.Date watchDate)
	{
		this.watchDate = watchDate;
	}
	public int getRunTime()
	{
		return runTime;
	}
	public void setRunTime(int runTime)
	{
		this.runTime = runTime;
	}
	public String getBasedOn()
	{
		return basedOn;
	}
	public void setBasedOn(String basedOn)
	{
		this.basedOn = basedOn;
	}
	public String getLanguage()
	{
		return language;
	}
	public void setLanguage(String language)
	{
		this.language = language;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public String getTrailer()
	{
		return trailer;
	}
	public void setTrailer(String trailer)
	{
		this.trailer = trailer;
	}
	@Override
	public String toString()
	{
		return "{ \"movieName\":\"" + movieName + "\", \"movieType\":\""
				+ movieType + "\", \"person\":\"" + person + "\", \"role\":\"" + role
				+ "\", \"releaseDate\":\"" + releaseDate + "\", \"watchDate\":\"" + watchDate
				+ "\", \"runTime\":\"" + runTime + "\", \"basedOn\":\"" + basedOn
				+ "\", \"language\":\"" + language + "\", \"country\":\"" + country
				+ "\", \"trailer\":\"" + trailer + "\"}";
	}
	
	
}
