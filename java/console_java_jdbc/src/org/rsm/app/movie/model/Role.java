package org.rsm.app.movie.model;

/**
 * @author RohitMuneshwar
 * This class represents person role in the movie
 */
public class Role
{
	// date
	long roleId;
	String name;
	public long getRoleId()
	{
		return roleId;
	}
	public void setRoleId(long roleId)
	{
		this.roleId = roleId;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	@Override
	public String toString()
	{
		return "{\"roleId\":\"" + roleId + "\", \"name\":\"" + name + "\"}";
	}
	
}
