package org.rsm.app.movie.dao.util;

import java.util.ArrayList;
import java.util.List;

import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;
import org.rsm.app.movie.util.IAppConstants;

/**
 * @author RohitMuneshwar
 * This is the utility class created for the DAO operations only
 */
public class DaoUtility 
{
	/**
	 * @param movie
	 * @return
	 * 
	 * Primary key will be auto-generated in the database table itself
	 * 
	 * format is as below:
	 * StringBuilder builder = new StringBuilder();
		builder.append("insert into movie")
		.append("(")
		.append(")")
		.append(" ")
		.append("values")
		.append("(")
		.append(")");
	 */
	public static String createInsertMovieQuery(Movie movie)
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.MOVIE) // table name - movie
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.MOVIE_ID_PK).append(IAppConstants.COMMA)	// movie_id_pk,
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA)	// name
		.append(IDaoConstants.MOVIE_TYPE).append(IAppConstants.COMMA)	// movie_type
		.append(IDaoConstants.BASED_ON_ID_FK).append(IAppConstants.COMMA)	// based_on_id_fk
		.append(IDaoConstants.RELEASE_DATE).append(IAppConstants.COMMA) // release date
		.append(IDaoConstants.WATCH_DATE).append(IAppConstants.COMMA) // watch date
		.append(IDaoConstants.RUN_TIME_IN_MINUTES).append(IAppConstants.COMMA) // runtime
		.append(IDaoConstants.RELEASE_COUNTRY_ID_FK).append(IAppConstants.COMMA) // release country id
		.append(IDaoConstants.LANGUAGE_ID_FK).append(IAppConstants.COMMA) // language id fk
		.append(IDaoConstants.TRAILER_LINK)	// trailer link
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(movie.getMovieId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(movie.getName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(movie.getMovieType()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(movie.getBasedOnId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(movie.getReleaseDate()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(movie.getWatchDate()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(movie.getRunTimeInMinutes()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(movie.getReleaseCountryId()).append(IAppConstants.COMMA)
		.append(movie.getLanguageId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(movie.getTrailerLink()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		return builder.toString();
	}

	/**
	 * @param book
	 * @return
	 * 
	 * primary key will be auto-generated in the database
	 */
	public static String createInsertBookQuery(Book book) 
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.BOOK) // table name - book
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.BOOK_ID_PK).append(IAppConstants.COMMA)	// book_id_pk
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA) // name
		.append(IDaoConstants.AUTHOR_ID_FK).append(IAppConstants.COMMA)// author_id_fk
		.append(IDaoConstants.PUBLISH_DATE) // publish_date
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(book.getBookId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(book.getName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(book.getAuthorId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(book.getPublishDate()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		return builder.toString();
	}

	/**
	 * @param country
	 * @return
	 * primary key will be auto-generated
	 */
	public static String createInsertCountryQuery(Country country) 
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.COUNTRY) // table name - country
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.COUNTRY_ID_PK).append(IAppConstants.COMMA)	// country_id_pk
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA) // name
		.append(IDaoConstants.SHORT_NAME) // shortname
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(country.getCountryId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(country.getName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(country.getShortName()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		return builder.toString();
	}

	/**
	 * @param language
	 * @return
	 * primary key will be auto-generated
	 */
	public static String createInsertLanguageQuery(Language language) 
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.LANGUAGE) // table name - language
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.LANGUAGE_ID_PK).append(IAppConstants.COMMA)	// language_id_pk
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA) // name
		.append(IDaoConstants.LANGUAGE_COUNTRY_ID_FK).append(IAppConstants.COMMA) // lang_country_id_fk
		.append(IDaoConstants.SHORT_NAME) // shortname
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(language.getLanguageId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(language.getName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(language.getCountryId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(language.getShortName()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		return builder.toString();
	}

	/**
	 * @param person
	 * @return
	 * Primary key will be auto-generated
	 */
	public static String createInsertPersonQuery(Person person) 
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.PERSON) // table name - person
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.PERSON_ID_PK).append(IAppConstants.COMMA)	// person_id_pk
		.append(IDaoConstants.FIRST_NAME).append(IAppConstants.COMMA) // first_name
		.append(IDaoConstants.MIDDLE_NAME).append(IAppConstants.COMMA) // middle_name
		.append(IDaoConstants.LAST_NAME).append(IAppConstants.COMMA) // last_name
		.append(IDaoConstants.NICK_NAME).append(IAppConstants.COMMA) // nick_name
		.append(IDaoConstants.GENDER).append(IAppConstants.COMMA) // gender
		.append(IDaoConstants.DATE_OF_BIRTH).append(IAppConstants.COMMA) // date_of_birth
		.append(IDaoConstants.WIKI_PAGE_LINK) // wiki_page
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(person.getPersonId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getFirstName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getMiddleName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getLastName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getNickName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getGender()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getDateOfBirth()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(person.getWikiPage()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		return builder.toString();
	}

	/**
	 * @param id
	 * @param tableName
	 * @return
	 * Creates and returns the delete query
	 */
	public static String createDeleteQuery(int id, String tableName, String pkColumnName) 
	{
		// code
		StringBuilder builder = new StringBuilder();
		builder.append(IDaoConstants.DELETE_FROM).append(IDaoConstants.SPACE) // delete from 
		.append(tableName).append(IDaoConstants.SPACE) // table_name
		.append(IDaoConstants.WHERE).append(IDaoConstants.SPACE) // where
		.append(pkColumnName).append(IDaoConstants.EQUAL_TO).append(id);	// column_name=id
		
		return builder.toString();
	}

	/**
	 * @param role
	 * @return
	 * creates and returns the insert role query
	 */
	public static String createInsertRoleQuery(Role role)
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.ROLE) // table name - role
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.ROLE_NAME)	// role_name
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(role.getName()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		
		return builder.toString();
	}

	/**
	 * @param company
	 * @return
	 * creates and returns the insert company query
	 */
	public static String createInsertCompanyQuery(Company company)
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
		.append(IDaoConstants.PD_COMPANY) // table name - pd_company
		.append(IAppConstants.OPEN_ROUND_BRACKET) // (
		.append(IDaoConstants.PRODUCTION_DISTRIBUTION_ID_PK).append(IAppConstants.COMMA)	// production_distribution_id_pk,
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA) // name
		.append(IDaoConstants.TYPE)	// type
		.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
		.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
		.append(company.getPdId()).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(company.getName()).append(IAppConstants.SINGLE_INVERTED_COMMA).append(IAppConstants.COMMA)
		.append(IAppConstants.SINGLE_INVERTED_COMMA).append(company.getType()).append(IAppConstants.SINGLE_INVERTED_COMMA)
		.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
		
		return builder.toString();
	}

	/**
	 * @param movie
	 * @return
	 * create list of queries for cast_movie mapping
	 */
	public static List<String> createInsertCastMovieMapQuery(Movie movie)
	{
		List<String> queries = new ArrayList<String>();
		
		for(Person person: movie.getStarcast())
		{
			for(Role role: person.getRoleIds())
			{
				StringBuilder builder = new StringBuilder();
				builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
				.append(IDaoConstants.CAST_MOVIE_MAP) // table name - cast_movie_map
				.append(IAppConstants.OPEN_ROUND_BRACKET) // (
				.append(IDaoConstants.MOVIE_ID_FK).append(IAppConstants.COMMA)	// movie_id_fk,
				.append(IDaoConstants.PERSON_ID_FK).append(IAppConstants.COMMA) // person_id_fk
				.append(IDaoConstants.ROLE_ID_FK)	// role_id_fk
				.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
				.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
				.append(movie.getMovieId()).append(IAppConstants.COMMA)
				.append(person.getPersonId()).append(IAppConstants.COMMA)
				.append(role.getRoleId())
				.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
				
				queries.add(builder.toString());
			}
		}
		return queries;
	}

	/**
	 * @param movie
	 * @return
	 * creates query for movie company map table
	 */
	/**
	 * @param movie
	 * @return
	 */
	public static List<String> createInsertMovieCompanyMapQuery(Movie movie)
	{
		List<String> queries = new ArrayList<String>();
		
		for(Company company: movie.getPdCompanies())
		{
				StringBuilder builder = new StringBuilder();
				builder.append(IDaoConstants.INSERT_INTO).append(IDaoConstants.SPACE) // insert into
				.append(IDaoConstants.MOVIE_COMPANY_MAP) // table name - movie_company_map
				.append(IAppConstants.OPEN_ROUND_BRACKET) // (
				.append(IDaoConstants.MOVIE_ID_FK).append(IAppConstants.COMMA).append(IDaoConstants.COMPANY_ID_FK) // movie_id_fk, company_id_fk
				.append(IAppConstants.CLOSE_ROUND_BRACKET).append(IDaoConstants.SPACE).append(IDaoConstants.VALUES)	// ) values
				.append(IAppConstants.OPEN_ROUND_BRACKET)	// (
				.append(movie.getMovieId()).append(IAppConstants.COMMA)
				.append(company.getPdId())	
				.append(IAppConstants.CLOSE_ROUND_BRACKET);	// )
				
				queries.add(builder.toString());
		}
		return queries;
	}

	/**
	 * @return
	 * It creates and return the query for choosing movies data from the database
	 * "select movie_id_pk, name, movie_type, based_on_id_fk, release_date, 
	 * watch_date, runtime_in_mins, release_country_id_fk, language_id_fk, trailer_link from movie";
	 */
	public static String createSelectMoviesQuery()
	{
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.MOVIE_ID_PK).append(IAppConstants.COMMA)	// movie_id_pk,
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA)	// name
		.append(IDaoConstants.MOVIE_TYPE).append(IAppConstants.COMMA)	// movie_type
		.append(IDaoConstants.BASED_ON_ID_FK).append(IAppConstants.COMMA)	// based_on_id_fk
		.append(IDaoConstants.RELEASE_DATE).append(IAppConstants.COMMA) // release date
		.append(IDaoConstants.WATCH_DATE).append(IAppConstants.COMMA) // watch date
		.append(IDaoConstants.RUN_TIME_IN_MINUTES).append(IAppConstants.COMMA) // runtime
		.append(IDaoConstants.RELEASE_COUNTRY_ID_FK).append(IAppConstants.COMMA) // release country id
		.append(IDaoConstants.LANGUAGE_ID_FK).append(IAppConstants.COMMA) // language id fk
		.append(IDaoConstants.TRAILER_LINK)	// trailer link
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.MOVIE) // table name
		; 
		return builder.toString();
	}

	/**
	 * @return
	 * "select m.name as 'MOVIE_NAME', m.movie_type as 'MOVIE_TYPE', concat(p.first_name,' ',p.last_name) as 'STAR CAST', r.role_name as 'ROLE',"+
										" m.release_date as 'RELEASE_DATE', m.watch_date as 'WATCH_DATE', m.runtime_in_mins as 'RUNTIME', b.name as 'BASED_ON', l.name as 'LANGUAGE', c.name as 'COUNTRY', m.trailer_link as 'TRAILER'"+
										" from movie m, book b, language l, country c, cast_movie_map cmm, person p, role r"+
										" where m.based_on_id_fk = b.book_id_pk"+
										" and m.movie_id_pk = cmm.movie_id_fk"+
										" and p.person_id_pk = cmm.person_id_fk"+
										" and r.role_id_pk = cmm.role_id_fk"+
										" and m.language_id_fk = l.language_id_pk"+
										" and m.release_country_id_fk = c.country_id_pk"
	 */
	public static String createQueryForMovieDetailsWithCast()
	{
		String str = "select m.name as 'MOVIE_NAME', m.movie_type as 'MOVIE_TYPE', concat(p.first_name,' ',p.last_name) as 'STAR CAST', r.role_name as 'ROLE',"+
				" m.release_date as 'RELEASE_DATE', m.watch_date as 'WATCH_DATE', m.runtime_in_mins as 'RUNTIME', b.name as 'BASED_ON', l.name as 'LANGUAGE', c.name as 'COUNTRY', m.trailer_link as 'TRAILER'"+
				" from movie m, book b, language l, country c, cast_movie_map cmm, person p, role r"+
				" where m.based_on_id_fk = b.book_id_pk"+
				" and m.movie_id_pk = cmm.movie_id_fk"+
				" and p.person_id_pk = cmm.person_id_fk"+
				" and r.role_id_pk = cmm.role_id_fk"+
				" and m.language_id_fk = l.language_id_pk"+
				" and m.release_country_id_fk = c.country_id_pk";
		return str;
	}

	/**
	 * @return
	 * "select m.name as 'MOVIE_NAME', m.movie_type as 'MOVIE_TYPE', m.release_date as 'RELEASE_DATE', m.watch_date as 'WATCH_DATE', m.runtime_in_mins as 'RUNTIME', b.name as 'BASED_ON', l.name as 'LANGUAGE', c.name as 'COUNTRY', m.trailer_link as 'TRAILER'"+
									" from movie m, book b, language l, country c"+
									" where m.based_on_id_fk = b.book_id_pk"+
									" and m.language_id_fk = l.language_id_pk"+
									" and m.release_country_id_fk = c.country_id_pk"
	 */
	public static String createQueryForMovieDetails()
	{
		String str = "select m.name as 'MOVIE_NAME', m.movie_type as 'MOVIE_TYPE', m.release_date as 'RELEASE_DATE', m.watch_date as 'WATCH_DATE', m.runtime_in_mins as 'RUNTIME', b.name as 'BASED_ON', l.name as 'LANGUAGE', c.name as 'COUNTRY', m.trailer_link as 'TRAILER'"+
				" from movie m, book b, language l, country c"+
				" where m.based_on_id_fk = b.book_id_pk"+
				" and m.language_id_fk = l.language_id_pk"+
				" and m.release_country_id_fk = c.country_id_pk";
		
		return str;
	}

	/**
	 * @return
	 * select book_id_pk, name, author_id_fk, publish_date from book
	 */
	public static String createSelectQueryForBooks()
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.BOOK_ID_PK).append(IAppConstants.COMMA)	// book_id_pk,
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA)	// name
		.append(IDaoConstants.AUTHOR_ID_FK).append(IAppConstants.COMMA)	// author_id_fk
		.append(IDaoConstants.PUBLISH_DATE)	// publish_date
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.BOOK) // table_name - book
		; 
		return builder.toString();
	}

	/**
	 * @return
	 * select country_id_pk, name, shortname from country
	 */
	public static String createSelectQueryForCountry()
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.COUNTRY_ID_PK).append(IAppConstants.COMMA)	// country_id_pk,
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA)	// name
		.append(IDaoConstants.SHORT_NAME)	// shortname
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.COUNTRY) // table_name - country
		; 
		return builder.toString();
	}

	/**
	 * @return
	 * select language_id_pk, name, lang_country_id_fk, shortname from language
	 */
	public static String createSelectQueryForLanguage()
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.LANGUAGE_ID_PK).append(IAppConstants.COMMA)	// language_id_pk,
		.append(IDaoConstants.NAME).append(IAppConstants.COMMA)	// name
		.append(IDaoConstants.LANGUAGE_COUNTRY_ID_FK).append(IAppConstants.COMMA)	// lang_country_id_fk
		.append(IDaoConstants.SHORT_NAME)	// shortname
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.LANGUAGE) // table_name - language
		; 
		return builder.toString();
	}

	/**
	 * @return
	 * select person_id_pk, first_name, middle_name, last_name, nick_name, gender, date_of_birth, wiki_page from person
	 */
	public static String createSelectQueryForPerson()
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.PERSON_ID_PK).append(IAppConstants.COMMA)	// person_id_pk,
		.append(IDaoConstants.FIRST_NAME).append(IAppConstants.COMMA)	// first_name
		.append(IDaoConstants.MIDDLE_NAME).append(IAppConstants.COMMA)	// middle_name
		.append(IDaoConstants.LAST_NAME).append(IAppConstants.COMMA)	// last_name
		.append(IDaoConstants.NICK_NAME).append(IAppConstants.COMMA)	// nick_name
		.append(IDaoConstants.GENDER).append(IAppConstants.COMMA)	// gender
		.append(IDaoConstants.DATE_OF_BIRTH).append(IAppConstants.COMMA)	// date_of_birth
		.append(IDaoConstants.WIKI_PAGE_LINK)	// wiki_page
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.PERSON) // table_name - person
		; 
		return builder.toString();
	}

	/**
	 * @return
	 * select role_id_pk, role_name from role
	 */
	public static String createSelectQueryForRole()
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.ROLE_ID_PK).append(IAppConstants.COMMA)	// role_id_pk,
		.append(IDaoConstants.ROLE_NAME)	// role_name
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.ROLE) // table_name - role
		; 
		return builder.toString();
	}

	/**
	 * @return
	 * select production_distribution_id_pk, name from pd_company
	 */
	public static String createSelectQueryForCompany()
	{
		// code
		StringBuilder builder = new StringBuilder();
		
		// prepare select clause
		builder.append(IDaoConstants.SELECT).append(IDaoConstants.SPACE) // select 
		.append(IDaoConstants.PRODUCTION_DISTRIBUTION_ID_PK).append(IAppConstants.COMMA)	// production_distribution_id_pk,
		.append(IDaoConstants.NAME)	// name
		.append(IDaoConstants.SPACE)
		// from clause
		.append(IDaoConstants.FROM).append(IDaoConstants.SPACE)
		.append(IDaoConstants.PD_COMPANY) // table_name - pd_company
		; 
		return builder.toString();
	}
}
