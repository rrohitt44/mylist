package org.rsm.app.movie.dao;

import java.util.List;

import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.MovieDetails;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;

/**
 * @author RohitMuneshwar
 * This is the interface for Movie Database Access Object.
 * 
 * It supports CRUD operations
 */
public interface IMovieDao 
{
	// Crud - Create
	public abstract int insertMovie(Movie movie);
	public abstract int insertBook(Book book);
	public abstract int insertCountry(Country country);
	public abstract int insertLanguage(Language language);
	public abstract int insertPerson(Person person);
	public abstract int insertRole(Role role);
	public abstract int insertCompany(Company company);
	
	// cRud - Read
	public abstract List<Movie> getMovies(); // load all the movies
	public abstract List<Book> getBooks(); // load all the books
	public abstract List<Country> getCountries(); // load all the countries
	public abstract List<Language> getLanguages(); // load all the languages
	public abstract List<Person> getPersons(); // load all the persons
	public abstract List<Role> getRoles(); // load all the roles
	public abstract List<Company> getCompanies(); // load all companies
	
	public abstract List<MovieDetails> getMovieDetails();
	public abstract List<MovieDetails> getMovieDetailsWithStarcast();
	
	// crUd - Update
	// To-DO
	
	// cruD - Delete
	public abstract int deleteMovie(int id);
	public abstract int deleteBook(int id);
	public abstract int deleteCountry(int id);
	public abstract int deleteLanguage(int id);
	public abstract int deletePerson(int id);
	public abstract int deleteRole(int id);
	public abstract int deleteCompany(int id);
	public abstract int deleteRecord(int id, String tableName, String pkColumnName);
}
