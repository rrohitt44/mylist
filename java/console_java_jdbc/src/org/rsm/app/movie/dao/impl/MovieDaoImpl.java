package org.rsm.app.movie.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.rsm.app.movie.dao.IMovieDao;
import org.rsm.app.movie.dao.cache.Cache;
import org.rsm.app.movie.dao.util.DaoUtility;
import org.rsm.app.movie.dao.util.IDaoConstants;
import org.rsm.app.movie.model.Book;
import org.rsm.app.movie.model.Company;
import org.rsm.app.movie.model.Country;
import org.rsm.app.movie.model.Language;
import org.rsm.app.movie.model.Movie;
import org.rsm.app.movie.model.MovieDetails;
import org.rsm.app.movie.model.Person;
import org.rsm.app.movie.model.Role;

/**
 * @author RohitMuneshwar
 *	Implementation class for IMovieDao.
 *	This is implementation for MySQL database.
 */
public class MovieDaoImpl implements IMovieDao 
{
	// data
	/**
	 * driver URL of the database which you wants to connect to
	 * The format is - jdbc:subprotocol:subname
	 * where subprotocol tells which driver to use and subname provides the driver
	 * with any required connection information
	 * e.g.
	 * mSQL database - jdbc:msql://carthage.imaginary.com/ora
	 * MySQL database - jdbc:mysql://localhost:3306/
	 * Java DB - jdbc:derby:testdb;create=true
	 */
	static String databaseUrl = "jdbc:mysql://localhost:3306/";
	
	/**
	 * username of the database
	 */
	static String username = "root";
	
	/**
	 *  password of the database
	 */
	static String password = "pass";
	
	/**
	 * driver class of the database used. 
	 * The driver class must implement java.sql.Driver interface.
	 * e.g.
	 * mSQL - com.imaginary.sql.msql.MsqlDriver
	 * Java DB - org.apache.derby.jdbc.EmbeddedDriver and org.apache.derby.jdbc.ClientDriver
	 * MySQL Connector/J - com.mysql.jdbc.Driver or com.mysql.cj.jdbc.Driver
	 */
	static String driverClass = "com.mysql.cj.jdbc.Driver";
	
	/**
	 * Name of the database to be used
	 */
	static String databaseName = "moviedb";
	
	// data caching
	static Cache cache = new Cache(); // for caching data
	
	// methods

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadMovies()
	 * load all the movies
	 */
	private List<Movie> loadMovies() 
	{
		System.out.println("inside loadMovies");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForMovies = DaoUtility.createSelectMoviesQuery();
		List<Movie> result = new ArrayList<Movie>();
		
		System.out.println("Generated query - "+queryForMovies);
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForMovies);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Movie movie = new Movie();
				movie.setMovieId(rs.getInt(1)); // primary key
				movie.setName(rs.getString(2)); // name
				movie.setMovieType(rs.getString(3)); // movie type
				movie.setBasedOnId(rs.getLong(4)); // based on id
				movie.setReleaseDate(rs.getTimestamp(5)); // release date
				movie.setWatchDate(rs.getTimestamp(6)); // watch date
				movie.setRunTimeInMinutes(rs.getInt(7)); // movie runtime
				movie.setReleaseCountryId(rs.getLong(8)); // country id
				movie.setLanguageId(rs.getLong(9)); // language id
				movie.setTrailerLink(rs.getString(10)); // trailer link
				result.add(movie);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadMovies");
		return result;
	}
	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadMovies()
	 * load all the movies
	 */
	private List<MovieDetails> loadMovieDetailsWithCast() 
	{
		System.out.println("inside loadMovieDetailsWithCast");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForMovieDetails = DaoUtility.createQueryForMovieDetailsWithCast();
		System.out.println("Generated query - "+queryForMovieDetails);
		List<MovieDetails> result = new ArrayList<MovieDetails>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForMovieDetails);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				MovieDetails movie = new MovieDetails();
				movie.setMovieName(rs.getString(1)); // movie name
				movie.setMovieType(rs.getString(2)); // movie type
				movie.setPerson(rs.getString(3)); // starcast
				movie.setRole(rs.getString(4)); // role
				movie.setReleaseDate(rs.getDate(5)); // release date
				movie.setWatchDate(rs.getDate(6)); // watch date
				movie.setRunTime(rs.getInt(7)); // movie runtime
				movie.setBasedOn(rs.getString(8)); // based on
				movie.setLanguage(rs.getString(9)); // language
				movie.setCountry(rs.getString(10)); // country
				movie.setTrailer(rs.getString(11)); // trailer link
				result.add(movie);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadMovieDetailsWithCast");
		return result;
	}
	
	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadMovies()
	 * load all the movies
	 */
	private List<MovieDetails> loadMovieDetails() 
	{
		System.out.println("inside loadMovieDetails");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForMovieDetails = DaoUtility.createQueryForMovieDetails();
		System.out.println("Generated query - "+queryForMovieDetails);
		List<MovieDetails> result = new ArrayList<MovieDetails>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForMovieDetails);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				MovieDetails movie = new MovieDetails();
				movie.setMovieName(rs.getString(1)); // movie name
				movie.setMovieType(rs.getString(2)); // movie type
				movie.setReleaseDate(rs.getDate(3)); // release date
				movie.setWatchDate(rs.getDate(4)); // watch date
				movie.setRunTime(rs.getInt(5)); // movie runtime
				movie.setBasedOn(rs.getString(6)); // based on
				movie.setLanguage(rs.getString(7)); // language
				movie.setCountry(rs.getString(8)); // country
				movie.setTrailer(rs.getString(9)); // trailer link
				result.add(movie);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadMovieDetails");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadBooks()
	 * Load all the books
	 */
	private List<Book> loadBooks()
	{
		System.out.println("inside loadBooks");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForBooks = DaoUtility.createSelectQueryForBooks();
		List<Book> result = new ArrayList<Book>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForBooks);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Book book = new Book();
				book.setBookId(rs.getInt(1)); // primary key
				book.setName(rs.getString(2)); // name
				book.setAuthorId(rs.getLong(3)); // get author id
				book.setPublishDate(rs.getTimestamp(4)); // publish date
				
				result.add(book);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadBooks");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadCountries()
	 * Load all the countries
	 */
	private List<Country> loadCountries() 
	{
		System.out.println("inside loadCountries");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForCountry = DaoUtility.createSelectQueryForCountry();
		List<Country> result = new ArrayList<Country>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForCountry);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Country country = new Country();
				country.setCountryId(rs.getInt(1)); // primary key
				country.setName(rs.getString(2)); // name
				country.setShortName(rs.getString(3)); // short name
				
				result.add(country);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadCountry");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadLanguages()
	 * Load all the languages
	 */
	private List<Language> loadLanguages() 
	{
		System.out.println("inside loadLanguages");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForLanguages = DaoUtility.createSelectQueryForLanguage();
		List<Language> result = new ArrayList<Language>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForLanguages);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Language language = new Language();
				language.setLanguageId(rs.getInt(1)); // primary key
				language.setName(rs.getString(2)); // name
				language.setCountryId(rs.getLong(3)); // country id
				language.setShortName(rs.getString(4)); // shortname
				
				result.add(language);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadLanguages");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadPersons()
	 * Load all the persons
	 */
	private List<Person> loadPersons() 
	{
		System.out.println("inside loadPersons");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForPersons = DaoUtility.createSelectQueryForPerson();
		List<Person> result = new ArrayList<Person>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForPersons);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Person person = new Person();
				person.setPersonId(rs.getInt(1)); // primary key
				person.setFirstName(rs.getString(2)); // first name
				person.setMiddleName(rs.getString(3)); // middle name
				person.setLastName(rs.getString(4)); // last name
				person.setNickName(rs.getString(5)); // nick name
				person.setGender(rs.getString(6)); // gender
				person.setDateOfBirth(rs.getTimestamp(7)); // date of birth
				person.setWikiPage(rs.getString(8)); // wiki-page
				
				result.add(person);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadPersons");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertMovie(org.rsm.app.movie.model.Movie)
	 * Inserts movie record in the database
	 */
	@Override
	public int insertMovie(Movie movie) 
	{
		System.out.println("inside insertMovie");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForMovie = DaoUtility.createInsertMovieQuery(movie);
		System.out.println("Generated Query - "+queryForMovie);
		
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// set auto-commit to false to handle multiple transactions - all or none
			conn.setAutoCommit(false);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing query for movie table ");
			retVal = stmt.executeUpdate(queryForMovie);
			System.out.println("Query for movie executed successfully");
			System.out.println("Query result - "+retVal);
			
			// create query for cast_movie_map table
			System.out.println("Executing query for cast_movie_map table ");
			List<String> queryForCastMovieMap = DaoUtility.createInsertCastMovieMapQuery(movie);
			// executing above queries
			for(int i=0; i<queryForCastMovieMap.size(); i++)
			{
				retVal = stmt.executeUpdate(queryForCastMovieMap.get(i));
				System.out.println("Query - "+queryForCastMovieMap.get(i));
				System.out.println((i+1)+" Query result - "+retVal);
			}
			System.out.println("Query for cast_movie_map executed successfully");
			
			// create and execute query for movie_company_map table
			System.out.println("Executing query for movie_company_map table ");
			List<String> queryForMovieCompanyMap = DaoUtility.createInsertMovieCompanyMapQuery(movie);
			// executing above queries
			for(int i=0; i<queryForMovieCompanyMap.size(); i++)
			{
				retVal = stmt.executeUpdate(queryForMovieCompanyMap.get(i));
				System.out.println("Query - "+queryForMovieCompanyMap.get(i));
				System.out.println((i+1)+" Query result - "+retVal);
			}
			System.out.println("Query for movie_company_map executed successfully");
			
			// commit the transaction now
			conn.commit();
			
			System.out.println("Movie commited in the database");
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertMovie");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertBook(org.rsm.app.movie.model.Book)
	 * Inserts book record in the database
	 */
	@Override
	public int insertBook(Book book) 
	{
		System.out.println("inside insertBook");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForBook = DaoUtility.createInsertBookQuery(book);
		System.out.println("Generated Query - "+queryForBook);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			retVal = stmt.executeUpdate(queryForBook);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertBook");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertCountry(org.rsm.app.movie.model.Country)
	 * Inserts country record in the database
	 */
	@Override
	public int insertCountry(Country country) 
	{
		System.out.println("inside insertCountry");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForCountry = DaoUtility.createInsertCountryQuery(country);
		System.out.println("Generated Query - "+queryForCountry);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			retVal = stmt.executeUpdate(queryForCountry);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertCountry");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertLanguage(org.rsm.app.movie.model.Language)
	 * Inserts language record in the database
	 */
	@Override
	public int insertLanguage(Language language) 
	{
		System.out.println("inside insertLanguage");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForLanguage = DaoUtility.createInsertLanguageQuery(language);
		System.out.println("Generated Query - "+queryForLanguage);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			retVal = stmt.executeUpdate(queryForLanguage);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertLanguage");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertPerson(org.rsm.app.movie.model.Person)
	 * Inserts person record in the database
	 */
	@Override
	public int insertPerson(Person person) 
	{
		System.out.println("inside insertPerson");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForPerson = DaoUtility.createInsertPersonQuery(person);
		System.out.println("Generated Query - "+queryForPerson);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			retVal = stmt.executeUpdate(queryForPerson);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertPerson");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteMovie(int)
	 */
	@Override
	public int deleteMovie(int id) 
	{
		return deleteRecord(id, IDaoConstants.MOVIE, IDaoConstants.MOVIE_ID_PK);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteBook(int)
	 */
	@Override
	public int deleteBook(int id) 
	{
		return deleteRecord(id, IDaoConstants.BOOK, IDaoConstants.BOOK_ID_PK);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteCountry(int)
	 */
	@Override
	public int deleteCountry(int id) 
	{
		return deleteRecord(id, IDaoConstants.COUNTRY, IDaoConstants.COUNTRY_ID_PK);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteLanguage(int)
	 */
	@Override
	public int deleteLanguage(int id) 
	{
		return deleteRecord(id, IDaoConstants.LANGUAGE, IDaoConstants.LANGUAGE_ID_PK);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deletePerson(int)
	 */
	@Override
	public int deletePerson(int id) 
	{
		return deleteRecord(id, IDaoConstants.PERSON, IDaoConstants.PERSON_ID_PK);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteRecord(int, java.lang.String)
	 */
	@Override
	public int deleteRecord(int id, String tableName, String pkColumnName) 
	{
		System.out.println("inside deleteRecord");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String deleteQuery = DaoUtility.createDeleteQuery(id, tableName,pkColumnName);
		System.out.println("Generated Query - "+deleteQuery);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing delete query");
			retVal = stmt.executeUpdate(deleteQuery);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from deleteRecord");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertRole(org.rsm.app.movie.model.Role)
	 * insert role to the database
	 */
	@Override
	public int insertRole(Role role)
	{
		System.out.println("inside insertRole");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForRole = DaoUtility.createInsertRoleQuery(role);
		System.out.println("Generated Query - "+queryForRole);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			retVal = stmt.executeUpdate(queryForRole);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertRole");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadRoles()
	 * load existing roles from the database
	 */
	private List<Role> loadRoles()
	{
		System.out.println("inside loadRoles");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForRole = DaoUtility.createSelectQueryForRole();
		List<Role> result = new ArrayList<Role>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForRole);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Role role = new Role();
				role.setRoleId(rs.getLong(1)); // primary key
				role.setName(rs.getString(2)); // name
				
				result.add(role);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadRoles");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteRole(int)
	 * delete existing role from the database
	 */
	@Override
	public int deleteRole(int id)
	{
		return deleteRecord(id, IDaoConstants.ROLE, IDaoConstants.ROLE_ID_PK);
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#insertCompany(org.rsm.app.movie.model.Company)
	 * insert company details into database
	 */
	@Override
	public int insertCompany(Company company)
	{
		System.out.println("inside insertCompany");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		int retVal = 0;
		String queryForCompany = DaoUtility.createInsertCompanyQuery(company);
		System.out.println("Generated Query - "+queryForCompany);
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			retVal = stmt.executeUpdate(queryForCompany);
			
			System.out.println("Checking result - "+retVal);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
		System.out.println("exiting from insertCompany");
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#loadCompanies()
	 * Load all companies from the database
	 */
	private List<Company> loadCompanies()
	{
		System.out.println("inside loadCompanies");
		// local variables
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String queryForCompany = DaoUtility.createSelectQueryForCompany();
		List<Company> result = new ArrayList<Company>();
		
		// code
		// load the Driver class; if driver class is not found in the classpath
		// ClassNotFoundException will be thrown
		try 
		{
			System.out.println("Loading the Driver manager class");
			// register JDCB driver with DriverManager
			Class.forName(driverClass).newInstance();
			
			System.out.println("Getting connection Using DriverManager");
			// if it is not able to get the connection then SQLException will be thrown
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			System.out.println("Creating statement");
			// create statement from connection object
			stmt = conn.createStatement();
			
			System.out.println("Executing Query");
			// execute the query using the statement
			rs = stmt.executeQuery(queryForCompany);
			
			System.out.println("Got Result:");
			
			// process the result one row at a time
			while(rs.next())
			{
				Company company = new Company();
				company.setPdId(rs.getLong(1)); // primary key
				company.setName(rs.getString(2)); // name
				
				result.add(company);
			}
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
			System.err.println("Can not instantiate driver manager class - "+driverClass);
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
			System.err.println("Can not access driver manager class - "+driverClass);
		} catch (ClassNotFoundException e) 
		{
			System.err.println(driverClass+" driver manager class not found");
			e.printStackTrace();
		}catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("SQL Exception occured while fetching data from database");
		}finally
		{
			try 
			{
				// closing resultset
				if(rs != null)
				{
					System.out.println("Closing result set");
					rs.close();
				}
				
				// closing statement
				if(stmt != null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				// close the connection if connection is not null
				if(conn != null)
				{
					
						System.out.println("Closing connection");
						conn.close();
					
				}
			} catch (SQLException e) 
			{
				e.printStackTrace();
				System.out.println("Error occured while closing the connection");
			}
		}
		System.out.println("exiting from the loadCompanies");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.rsm.app.movie.dao.IMovieDao#deleteCompany(int)
	 * Delete company from the database
	 */
	@Override
	public int deleteCompany(int id)
	{
		return deleteRecord(id, "pd_company", "production_distribution_id_pk");
	}

	@Override
	public List<Movie> getMovies()
	{
		if(cache.get(Cache.MOVIE_CACHE) == null || Cache.stateChangedMovie)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedMovie = false;
			cache.put(Cache.MOVIE_CACHE, loadMovies());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Movie>) cache.get(Cache.MOVIE_CACHE);
	}

	@Override
	public List<Book> getBooks()
	{
		if(cache.get(Cache.BOOK_CACHE) == null || Cache.stateChangedBook)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedBook = false;
			cache.put(Cache.BOOK_CACHE, loadBooks());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Book>) cache.get(Cache.BOOK_CACHE);
	}

	@Override
	public List<Country> getCountries()
	{
		if(cache.get(Cache.COUNTRY_CACHE) == null || Cache.stateChangedCountry)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedCountry = false;
			cache.put(Cache.COUNTRY_CACHE, loadCountries());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Country>) cache.get(Cache.COUNTRY_CACHE);
	}

	@Override
	public List<Language> getLanguages()
	{
		if(cache.get(Cache.LANGUAGE_CACHE) == null || Cache.stateChangedLanguage)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedLanguage = false;
			cache.put(Cache.LANGUAGE_CACHE, loadLanguages());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Language>) cache.get(Cache.LANGUAGE_CACHE);
	}

	@Override
	public List<Person> getPersons()
	{
		if(cache.get(Cache.PERSON_CACHE) == null || Cache.stateChangedPerson)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedPerson = false;
			cache.put(Cache.PERSON_CACHE, loadPersons());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Person>) cache.get(Cache.PERSON_CACHE);
	}

	@Override
	public List<Role> getRoles()
	{
		if(cache.get(Cache.ROLE_CACHE) == null || Cache.stateChangedRole)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedRole = false;
			cache.put(Cache.ROLE_CACHE, loadRoles());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Role>) cache.get(Cache.ROLE_CACHE);
	}

	@Override
	public List<Company> getCompanies()
	{
		if(cache.get(Cache.PD_COMPANY_CACHE) == null || Cache.stateChangedPDCompany)
		{
			System.out.println("loading fresh data from db");
			Cache.stateChangedPDCompany = false;
			cache.put(Cache.PD_COMPANY_CACHE, loadCompanies());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<Company>) cache.get(Cache.PD_COMPANY_CACHE);
	}

	@Override
	public List<MovieDetails> getMovieDetails()
	{
		if(cache.get(Cache.MOVIE_DETAILS_CACHE) == null || Cache.stateChangedMovie)
		{
			System.out.println("loading fresh data from db");
			cache.put(Cache.MOVIE_DETAILS_CACHE, loadMovieDetails());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<MovieDetails>) cache.get(Cache.MOVIE_DETAILS_CACHE);
	}

	@Override
	public List<MovieDetails> getMovieDetailsWithStarcast()
	{
		if(cache.get(Cache.MOVIE_DETAILS_WITH_STARCAST_CACHE) == null)
		{
			System.out.println("loading fresh data from db");
			cache.put(Cache.MOVIE_DETAILS_WITH_STARCAST_CACHE, loadMovieDetailsWithCast());
		}else
		{
			System.out.println("Data already available in cache so no need to fetch from db");
		}
		return (List<MovieDetails>) cache.get(Cache.MOVIE_DETAILS_WITH_STARCAST_CACHE);
	}
}
