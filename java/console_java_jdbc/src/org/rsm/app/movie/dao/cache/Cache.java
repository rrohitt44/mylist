package org.rsm.app.movie.dao.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * @author RohitMuneshwar
 * This is a cache class which helps us minimize the database hits
 */
public class Cache
{
	// variables for cache management
	public static final String MOVIE_CACHE = "movie_cache";
	public static final String PERSON_CACHE = "person_cache";
	public static final String ROLE_CACHE = "role_cache";
	public static final String COUNTRY_CACHE = "country_cache";
	public static final String LANGUAGE_CACHE = "language_cache";
	public static final String BOOK_CACHE = "book_cache";
	public static final String PD_COMPANY_CACHE = "company_cache";
	public static final String MOVIE_DETAILS_CACHE = "movie_details_cache";
	public static final String MOVIE_DETAILS_WITH_STARCAST_CACHE = "movie_details_with_starcast_cache";
	
	public static boolean stateChangedMovie = false;
	public static boolean stateChangedPerson = false;
	public static boolean stateChangedRole = false;
	public static boolean stateChangedCountry = false;
	public static boolean stateChangedLanguage = false;
	public static boolean stateChangedBook = false;
	public static boolean stateChangedPDCompany = false;
	
	// cache variable used to hold the cached data
	private static Map<String, Object> cache = new HashMap<String, Object>();
	
	/**
	 * @param key
	 * @return
	 * This returns the object based on the key
	 */
	public Object get(String key)
	{
		return cache.get(key);
	}
	
	/**
	 * @param key
	 * @param value
	 * Stores an object and key in the cache
	 */
	public void put(String key, Object value)
	{
		cache.put(key, value);
	}
}
