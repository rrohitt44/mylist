package org.rsm.app.movie.dao.util;

/**
 * @author RohitMuneshwar
 * This is for declaring all the constants used in the DAO layer
 */
public interface IDaoConstants
{
	// query constants
	String SELECT = "select";
	String FROM = "from";
	String WHERE = "where";
	String AND = "and";
	String OR = "or";
	String TABLE = "table";
	String INTO = "into";
	String STAR = "*";
	String SPACE = " ";
	String INSERT = "insert";
	String INSERT_INTO = "insert into";
	String DELETE_FROM = "delete from";
	String VALUES = "values";
	String EQUAL_TO = "=";
	
	// table/column names
	String BOOK = "book";
	String BOOK_ID_PK = "book_id_pk";
	String NAME = "name";
	String AUTHOR_ID_FK = "author_id_fk";
	String PUBLISH_DATE = "publish_date";
	
	String CAST_MOVIE_MAP = "cast_movie_map";
	String CAST_MOVIE_MAP_ID_PK = "cast_movie_map_id_pk";
	String MOVIE_ID_FK = "movie_id_fk";
	String PERSON_ID_FK ="person_id_fk";
	String ROLE_ID_FK = "role_id_fk";
	
	String COUNTRY = "country";
	String COUNTRY_ID_PK = "country_id_pk";
	String SHORT_NAME = "shortname";
	
	String LANGUAGE = "language";
	String LANGUAGE_ID_PK = "language_id_pk";
	String LANGUAGE_COUNTRY_ID_FK = "lang_country_id_fk";
	
	String MOVIE = "movie";
	String MOVIE_ID_PK = "movie_id_pk";
	String MOVIE_TYPE = "movie_type";
	String BASED_ON_ID_FK = "based_on_id_fk";
	String RELEASE_DATE = "release_date";
	String WATCH_DATE = "watch_date";
	String RUN_TIME_IN_MINUTES = "runtime_in_mins";
	String RELEASE_COUNTRY_ID_FK = "release_country_id_fk";
	String LANGUAGE_ID_FK = "language_id_fk";
	String TRAILER_LINK = "trailer_link";
	
	String MOVIE_COMPANY_MAP = "movie_company_map";
	String MOVIE_COMPANY_MAP_ID_PK = "person_company_map_id_pk";
	String COMPANY_ID_FK = "company_id_fk";
	
	String PD_COMPANY = "pd_company";
	String PRODUCTION_DISTRIBUTION_ID_PK = "production_distribution_id_pk";
	String TYPE = "type";
	
	String PERSON = "PERSON";
	String PERSON_ID_PK = "person_id_pk";
	String FIRST_NAME = "first_name";
	String MIDDLE_NAME = "middle_name";
	String LAST_NAME = "last_name";
	String NICK_NAME = "nick_name";
	String GENDER = "gender";
	String DATE_OF_BIRTH = "date_of_birth";
	String WIKI_PAGE_LINK = "wiki_page";
	
	String ROLE = "role";
	String ROLE_ID_PK = "role_id_pk";
	String ROLE_NAME = "role_name";
}
