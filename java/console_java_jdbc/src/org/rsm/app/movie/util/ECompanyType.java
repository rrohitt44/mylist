package org.rsm.app.movie.util;

public enum ECompanyType
{
	PRODUCTION_COMPANY(1), DISTRIBUTION_COMPANY(2);
	
	private int value;
	
	private ECompanyType(int value)
	{
		this.value = value;
	}
	
	public int getValue()
	{
		return value;
	}
}
