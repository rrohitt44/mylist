package org.rsm.app.movie.util;

public interface IAppConstants
{
	public static final String COMMA = ",";
	public static final String OPEN_ROUND_BRACKET = "(";
	public static final String CLOSE_ROUND_BRACKET = ")";
	public static final String OPEN_CURLY_BRACKET = "{";
	public static final String CLOSE_CURLY_BRACKET = "}";
	public static final String OPEN_SQUARE_BRACKET = "[";
	public static final String CLOSE_SQUARE_BRACKET = "]";
	public static final String SINGLE_INVERTED_COMMA = "'";
	public static final String DOUBLE_INVERTED_COMMA = "\"";
	public static final String SEP_BAR = "\\|";
	
	// role related
	String ROLE_NAME="rolename";
	
	// movie related
	String MOVIE_NAME = "name";
	String MOVIE_TYPE = "type";
	String RELEASE_DATE = "releasedate";
	String WATCH_DATE = "watchdate";
	String RUN_TIME = "runtime";
	String COUNTRY = "country";
	String LANGUAGE = "language";
	String BASED_ON = "basedon";
	String TRAILER_LINK = "trailerlink";
	String PERSON = "personname";
	String COMPANY_NAME = "companyname";
	
	// person related
	String FIRST_NAME = "firstname";
	String MIDDLE_NAME = "middlename";
	String LAST_NAME = "lastname";
	String NICK_NAME = "nickname";
	String GENDER = "gender";
	String DATE_OF_BIRTH = "dob";
	String WIKI_PAGE = "wikipage";
	
	// book related
	String BOOK_NAME = "bookname";
	String PUBLISH_DATE = "publishdate";
	
	// country related
	String COUNTRY_NAME = "countryname";
	String SHORT_NAME = "shortname";
	
	// language related
	String LANUGAGE_NAME = "languagename";
	String LANG_COUNTRY_ID = "languagecountry";
	
	// company related
	String COMPANY_TYPE = "companytype";
}
