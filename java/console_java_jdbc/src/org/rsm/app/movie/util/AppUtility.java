package org.rsm.app.movie.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

/**
 * @author RohitMuneshwar
 * This class is for the utilities needed for the application
 */
public class AppUtility
{
	/**
	 * @return
	 * This method gets the input from the user and returns the timestamp value of it
	 */
	public static java.sql.Timestamp getTimestampFromConsole()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Day:");
		int day = sc.nextInt();
		System.out.println("Enter Month:");
		int month = sc.nextInt();
		System.out.println("Enter Year:");
		int year = sc.nextInt();
		GregorianCalendar gCal =new GregorianCalendar(year,month,day,00,00,00);
		long value = gCal.getTimeInMillis();
		java.sql.Timestamp ts = new Timestamp(value);
		return ts;
	}
	
	/**
	 * @return
	 * This method gets the input from the user and returns the timestamp value of it
	 */
	public static java.sql.Date getDateFromConsole()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Day:");
		int day = sc.nextInt();
		System.out.println("Enter Month:");
		int month = sc.nextInt();
		System.out.println("Enter Year:");
		int year = sc.nextInt();
		GregorianCalendar gCal =new GregorianCalendar(year,month,day,00,00,00);
		long value = gCal.getTimeInMillis();
		java.sql.Date ts = new java.sql.Date(value);
		return ts;
	}

	/**
	 * @return
	 * Get line from console
	 */
	public static String getLine()
	{
		Scanner sc =new Scanner(System.in);
		StringBuilder builder=new StringBuilder(); 
		builder.append(sc.nextLine());
		sc = null;
		return builder.toString();
	}
	
	/**
	 * @param list
	 * @return
	 * Convert the list to json; provided toString of pojo classes override in json format
	 */
	public static String convertToJson(List list)
	{
		StringBuilder builder = new StringBuilder();
		
		// iterate and add in json format
		builder.append(IAppConstants.OPEN_SQUARE_BRACKET);
		if(list != null)
		{
			for(Object obj : list)
			{
				builder.append(obj).append(IAppConstants.COMMA);
			}
			
			// remove last comma
			builder.setLength(builder.length() - 1);
		}
		
		builder.append(IAppConstants.CLOSE_SQUARE_BRACKET);
		return builder.toString();
	}

	public static String convertToJson(int result)
	{
		String res = "";
		if(result != 0)
		{
			res = "{\"success\":\"true\"}";
		}else
		{
			res = "{\"success\":\"false\"}";
		}
		return res;
	}
	
	/**
	 * @param dateStr
	 * @return
	 * Convert string to date
	 */
	public static java.sql.Date convertToDate(String dateStr)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d;
		try
		{
			d = format.parse(dateStr);
		} catch (ParseException e)
		{
			d = new Date();
			e.printStackTrace();
		}
		
		return new java.sql.Date(d.getTime());
	}
	
	/**
	 * @param dateStr
	 * @return
	 * Convert string to date
	 */
	public static java.sql.Timestamp convertToTimestamp(String dateStr)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d;
		try
		{
			d = format.parse(dateStr);
		} catch (ParseException e)
		{
			d = new Date();
			e.printStackTrace();
		}
		
		return new java.sql.Timestamp(d.getTime());
	}
}
