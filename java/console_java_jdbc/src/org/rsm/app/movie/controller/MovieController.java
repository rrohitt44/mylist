package org.rsm.app.movie.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.rsm.app.movie.service.IMovieService;

/**
 * @author RohitMuneshwar
 * This is the controller class for movies.
 */
public class MovieController 
{
	private IMovieService movieService; // service class for movies
	// data
	/**
	 * driver URL of the database which you wants to connect to
	 * The format is - jdbc:subprotocol:subname
	 * where subprotocol tells which driver to use and subname provides the driver
	 * with any required connection information
	 * e.g.
	 * mSQL database - jdbc:msql://carthage.imaginary.com/ora
	 * MySQL database - jdbc:mysql://localhost:3306/
	 * Java DB - jdbc:derby:testdb;create=true
	 */
	static String databaseUrl = "jdbc:mysql://localhost:3306/";
	
	/**
	 * username of the database
	 */
	static String username = "root";
	
	/**
	 *  password of the database
	 */
	static String password = "pass";
	
	/**
	 * driver class of the database used. 
	 * The driver class must implement java.sql.Driver interface.
	 * e.g.
	 * mSQL - com.imaginary.sql.msql.MsqlDriver
	 * Java DB - org.apache.derby.jdbc.EmbeddedDriver and org.apache.derby.jdbc.ClientDriver
	 * MySQL Connector/J - com.mysql.jdbc.Driver or com.mysql.cj.jdbc.Driver
	 */
	static String driverClass = "com.mysql.cj.jdbc.Driver";
	
	/**
	 * Name of the database to be used
	 */
	static String databaseName = "moviedb";
	/**
	 * inserts new movie record in the database
	 */
	public static void insertMovie()
	{
		// local variables
		Connection conn = null;
		Statement stmt = null;
		
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			int success = stmt.executeUpdate("insert into movie(name, movie_type) values('Khiladi 360', 'Bollywood')");
			
			System.out.println("Checking result - "+success);
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(stmt!= null)
				{
					System.out.println("Closing statement");
					stmt.close();
				}
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * this is to insert multiple statements and finally commit the transaction
	 * At the start auto-commit is set to false so that the default behavior, which commits
	 * every statement automatically, is overridden.
	 */
	public static void insertMultiple()
	{
		// local variables
		Connection conn = null;
		
		try
		{
			// register java.sql.Driver class in driver manager
			System.out.println("Loading driver class");
			Class.forName(driverClass).newInstance();
			
			// create connection using the Driver manager
			System.out.println("Creating connection");
			conn = DriverManager.getConnection(databaseUrl+databaseName, username, password);
			
			// set auto-commit to false
			conn.setAutoCommit(false);
			
			// first statement
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			Statement stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			int success = stmt.executeUpdate("insert into movie(name, movie_type) values('Khiladi 360', 'Bollywood')");
			
			System.out.println("Closing first statement");
			stmt.close();
			
			// second statement
			// create statement using connection for executing the queries
			System.out.println("Creating statement");
			stmt = conn.createStatement();
			
			// execute update query now; executeUpdate returns the number of rows updated
			System.out.println("Executing update query");
			success += stmt.executeUpdate("insert into movie(name, movie_type) values('Khiladi 360', 'Bollywood')");
			
			System.out.println("Closing second statement");
			
			// commit both statements
			System.out.println("Committing both statements");
			conn.commit();
			
			stmt.close();
			
			System.out.println("Insersion successful with "+success+" transactions.");
		}catch(ClassNotFoundException e)
		{
			System.err.println("Error while loading driver class");
			e.printStackTrace();
		} catch (InstantiationException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} catch (SQLException e) 
		{
			System.err.println("Sql exception occured");
			e.printStackTrace();
		}finally // cleanup
		{
			try 
			{
				if(conn!=null)
				{
					System.out.println("Closing connection");
					conn.close();
				}
			} catch (SQLException e) 
			{
				System.err.println("Error while cleanup");
				e.printStackTrace();
			}
		}
	}
}
