create table movie_company_map
(
	person_company_map_id_pk int auto_increment,
	movie_id_fk int,
	company_id_fk int,
	PRIMARY KEY (person_company_map_id_pk),
	FOREIGN KEY (company_id_fk) REFERENCES pd_company(production_distribution_id_pk),
	FOREIGN KEY (movie_id_fk) REFERENCES movie(movie_id_pk)
);