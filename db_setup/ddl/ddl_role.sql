create table role
(
	role_id_pk int auto_increment,
	role_name varchar(255) not null unique,
	PRIMARY KEY (role_id_pk)
);