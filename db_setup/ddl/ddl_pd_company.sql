create table pd_company
(
	production_distribution_id_pk int auto_increment,
	name varchar(255) not null unique,
	type int not null,
	PRIMARY KEY (production_distribution_id_pk)
);