create table language
(
	language_id_pk int auto_increment,
	name varchar(50) default 'Marathi',
	lang_country_id_fk int,
	shortname char(10) default 'MAR',
	PRIMARY KEY (language_id_pk),
	FOREIGN KEY (lang_country_id_fk) REFERENCES COUNTRY(country_id_pk)
);

