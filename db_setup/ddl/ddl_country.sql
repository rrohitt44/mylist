create table country
(
	country_id_pk int auto_increment,
	name varchar(50) default 'india',
	shortname char(10) default 'IN',
	PRIMARY KEY (country_id_pk)
);