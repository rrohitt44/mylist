CREATE TABLE PERSON
(
	person_id_pk int auto_increment,
	first_name varchar(50) not null,
	middle_name varchar(50) DEFAULT NULL,
	last_name varchar(50) not null,
	nick_name varchar(50) DEFAULT NULL,
	gender varchar(30),
	date_of_birth TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	wiki_page varchar(255) DEFAULT NULL,
	PRIMARY KEY(person_id_pk)
);

