create table movie
(
	movie_id_pk int auto_increment,
	name varchar(255) not null,
	movie_type char(30) not null,
	based_on_id_fk int,
	release_date TIMESTAMP not null,
	watch_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	runtime_in_mins int,
	release_country_id_fk int,
	language_id_fk int,
	trailer_link varchar(255),
	PRIMARY KEY (movie_id_pk),
	foreign key (based_on_id_fk) references book(book_id_pk),
	foreign key (release_country_id_fk) references country(country_id_pk),
	foreign key (language_id_fk) references language(language_id_pk)
);