create table cast_movie_map
(
	cast_movie_map_id_pk int auto_increment,
	movie_id_fk int,
	person_id_fk int,
	role_id_fk int,
	PRIMARY KEY (cast_movie_map_id_pk),
	FOREIGN KEY (movie_id_fk) REFERENCES movie(movie_id_pk),
	FOREIGN KEY (person_id_fk) REFERENCES person(person_id_pk),
	FOREIGN KEY (role_id_fk) REFERENCES role(role_id_pk)
);