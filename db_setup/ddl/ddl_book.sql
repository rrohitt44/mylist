create table book
(
	book_id_pk int auto_increment,
	name varchar(255) not null,
	author_id_fk int,
	publish_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (book_id_pk),
	FOREIGN KEY (author_id_fk) REFERENCES person(person_id_pk)
);