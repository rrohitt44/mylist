DECLARE 
   -- variable declaration 
   message  varchar2(20):= 'Hello, World!'; 
BEGIN 
   /* 
   *  PL/SQL executable statement(s) 
   */ 
   dbms_output.put_line(message); 
END; 
-- To run the code from the SQL command line, you may need to type / at the beginning of the first blank line after the last line of the code.
/