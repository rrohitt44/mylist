// Get Movie details along with star cast and roles
select m.name as "MOVIE_NAME", m.movie_type as "MOVIE_TYPE", concat(p.first_name,' ',p.last_name) as "STAR CAST", r.role_name as "ROLE",
m.release_date as "RELEASE_DATE", m.watch_date as "WATCH_DATE", m.runtime_in_mins as "RUNTIME", b.name as "BASED_ON", l.name as "LANGUAGE", c.name as "COUNTRY", m.trailer_link as "TRAILER"
from movie m, book b, language l, country c, cast_movie_map cmm, person p, role r
where m.based_on_id_fk = b.book_id_pk
and m.movie_id_pk = cmm.movie_id_fk
and p.person_id_pk = cmm.person_id_fk
and r.role_id_pk = cmm.role_id_fk
and m.language_id_fk = l.language_id_pk
and m.release_country_id_fk = c.country_id_pk
;

// Get movie list with details only
select m.name as "MOVIE_NAME", m.movie_type as "MOVIE_TYPE", m.release_date as "RELEASE_DATE", m.watch_date as "WATCH_DATE", m.runtime_in_mins as "RUNTIME", b.name as "BASED_ON", l.name as "LANGUAGE", c.name as "COUNTRY", m.trailer_link as "TRAILER"
from movie m, book b, language l, country c
where m.based_on_id_fk = b.book_id_pk
and m.language_id_fk = l.language_id_pk
and m.release_country_id_fk = c.country_id_pk
;