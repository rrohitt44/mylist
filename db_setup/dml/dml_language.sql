insert into language(name, lang_country_id_fk, shortname) values('Hindi', 1, 'Hn');
insert into language(name, lang_country_id_fk, shortname) values('Marathi', 1, 'Mar');
insert into language(name, lang_country_id_fk, shortname) values('English', 1, 'Eng');
insert into language(name, lang_country_id_fk, shortname) values('English', 2, 'Eng');
insert into language(name, lang_country_id_fk, shortname) values('English', 4, 'Eng');
insert into language(name, lang_country_id_fk, shortname) values('English', 3, 'Eng');
insert into language(name, lang_country_id_fk, shortname) values('Telgu', 1, 'Tel');
insert into language(name, lang_country_id_fk, shortname) values('Tamil', 1, 'Tam');
