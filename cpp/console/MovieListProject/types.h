#ifndef H_TYPES
#define H_TYPES

// headers
#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<ostream>
#include<istream>
#include<stdio.h>
#include<cstring>
#include"logger.h"

using namespace std;

// data

// global variable declarations
template<class T> class Vec : public vector<T>
{
	public:
	Vec() : vector<T>() {}
	Vec(int s) : vector<T>(s) {}
	T& operator[] (int i) { return this->at(i);} // range-checked
	const T& operator[] (int i) const { return this->at(i); } // range-checked
};

class Date
{
	public: // public interface
	enum Month {JAN=1, FEB, MAR, APRL, MAY, JUN, JUL, AUG, SEPT, OCT, NOV, DEC};
	class BadDate {}; // exception class

	Date(int dd=0, Month mm = Month(0), int yy=0);
	//Date(int dd=0, int mm=0, int yy=0);

	// functions for examining date
	int day() const;
	Month month() const;
	int year() const;
	static void setDefault(int, Month, int);

	// modify date functions
	Date& addDay(int d);
	Date& addMonth(int m);
	Date& addYear(int y);
	
	string stringRep() const; // string representation
	void charRep(char s[]) const; // c-style string representation

	private:
	int d, m, y;
	mutable bool cacheValid; // cache
	mutable string cache; // cached data
	void computeCacheValue() const; // fill cache
	static Date defaultDate;
};

class MovieInfo
{
	public: // public interface
	enum MOVIE_TYPE
	{
		HOLLYWOOD,BOLLYWOOD,MARATHI, TOLLYWOOD
	};
	
	enum MENU_TYPE
	{
		MAIN_MENU, MOVIE_TYPE_MENU, MOVIE_FIELD_MENU
	};

	// constructor
	MovieInfo(Date d);

	// functions for examination/getters
	int getType() const;
	string getName() const;
	string getDirectors() const;
	string getWriters() const;
	string getMusicDirectors() const;
	string getCinematographers() const;
	string getEditors() const;
	string getCasts() const;
	string getProductionCompanyName() const;
	string getDistributors() const;
	Date getReleaseDate() const;
	string getWatchDate() const;

	// functions for changing the data
	MovieInfo& setType(int type);
	MovieInfo& setName(string name);
	MovieInfo& setDirectors(string name);
	MovieInfo& setWriters(string name);
	MovieInfo& setMusicDirectors(string name);
	MovieInfo& setCinematographers(string name);
	MovieInfo& setEditors(string name);
	MovieInfo& setCasts(string name);
	MovieInfo& setProductionCompanyNames(string name);
	MovieInfo& setDistributors(string name);
	MovieInfo& setReleaseDate(Date name);
	MovieInfo& setWatchDate(string name);

	// representations
	private:
	int type;
	string name;
	string directed_by;
	string written_by;
	string music_by;
	string cinematography_by;
	string edited_by;
	string starring;
	string production_company_name;
	string distributed_by;
	Date release_date;
	string watch_date;
};

extern Vec<MovieInfo> gMovielist; // this is used to store data from the db globally
extern string flushy; // this is used to flush buffer
extern string movieListFileName; // database file name
extern string SEP_BAR; // BAR seperator
extern char* cpNULL; // end of the arguments for VAR-ARGS
extern int giERROR; // Error flag if any error occured
extern string SEP_COMMA; // comma seperator
#endif
