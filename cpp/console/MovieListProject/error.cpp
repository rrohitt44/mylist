#include"types.h"
#include"error.h"

char* cpNULL = 0;
int giERROR = 0;

int numberOfErrors = 0;

/*
	error:
	compose error message by passing each word as a seperate string argument
	the argument list is terminated by a character pointed 0
*/
void Error::error(int severity ...)
{
	va_list ap; // list of variable list arguments
	va_start(ap, severity); // initialization of the list

	for(;;)
	{
	 	char* p = va_arg(ap, char*); // each argument
		
		// if end of the list comes then break the loop
		if(p==0)
		{
			break;
		}
		cout << p << endl; // print the argument/message
	}

	// va_end() is must; the reason is that va_start() may modify the stack in such a way that
	// a return cannot successfully be done; va_end() undoes any such modifications
	va_end(ap);  // arg cleanup
	cout << endl;
	if(severity)
	{
		exit(severity);
	}
}
	
double Error::error(const char* s)
{
	cerr << "error: " << s << endl;
	numberOfErrors++;
	return 1;
}
