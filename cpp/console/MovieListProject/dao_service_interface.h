#ifndef H_DAO_SERVICE_INTERFACE
#define H_DAO_SERVICE_INTERFACE

// headers
#include"dao.h"
#include"menu_service_interface.h"
#include"types.h"

namespace DAOServiceInterface
{
	void loadRecords(); // read all
	int addRecord(MovieInfo movieInfo); // create
	int deleteRecord(string name); // delete
	int updateRecord(int index); // update

	using namespace std;
}


#endif
