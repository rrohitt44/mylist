#ifndef H_MENU_SERVICE_INTERFACE
#define H_MENU_SERVICE_INTERFACE

// headers
#include<iostream>
#include<stdio.h>
#include"display.h"

// for displaying various menus
namespace MenuServiceInterface
{
	void view(MovieInfo::MENU_TYPE type);
	void viewMovieDetails(int i);
	void showMovieList();
	void showAllMovieDetails();
	string chooseMovieName();
	MovieInfo getMovieRecord();
	int showMovieListAndChooseTheOne();

	using namespace std;
}
#endif
