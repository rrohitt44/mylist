#ifndef H_ERROR
#define H_ERROR

namespace Error
{
	void error(int ...);
	double error(const char* s);
}
#endif
