#include "menu_service_interface.h"

void MenuServiceInterface::view(MovieInfo::MENU_TYPE type)
{
	Display::view(type);	
}

MovieInfo MenuServiceInterface::getMovieRecord()
{
	return Display::getMovieRecord();	
}

void MenuServiceInterface::showMovieList()
{
	Display::showMovieList();
}

void MenuServiceInterface::viewMovieDetails(int index)
{
	Display::printMovieEntry(index);
}

void MenuServiceInterface::showAllMovieDetails()
{
	Display::printAllMovies();
}

string MenuServiceInterface::chooseMovieName()
{
	return Display::chooseMovieName();
}
int MenuServiceInterface::showMovieListAndChooseTheOne()
{
	return Display::showMovieListAndChooseTheOne();
}
