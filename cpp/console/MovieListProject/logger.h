#ifndef H_LOGGER
#define H_LOGGER

#include<stdio.h>

namespace Logger
{
	using namespace std;

		enum LOGGING_MODE{FILE, CONSOLE};
		void init(); // constructor
		void close(); // destructor
				// state analyzing methods

		// state changin methods

		// logging methods
		void log(string str);
		void debug(string str);
		void info(string str);
		void error(string str);
		void exception(string str);
}
#endif
