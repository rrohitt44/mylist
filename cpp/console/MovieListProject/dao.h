#ifndef H_DAO
#define H_DAO

#include"types.h"
#include<iostream>
#include"utilities.h"
#include"logger.h"

namespace Repository
{
	void readAll();
	void create(MovieInfo record);
	void update(MovieInfo record);
	string createRecordString(MovieInfo record);
	void deleteRecord(string movieName);

	using namespace std;
}

#endif
