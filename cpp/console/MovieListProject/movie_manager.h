#ifndef H_MOVIE_MANAGER
#define H_MOVIE_MANAGER

// headers
#include"types.h"
#include"menu_service_interface.h"
#include"dao_service_interface.h"
#include"utilities.h"
#include"logger.h"

// namespaces
namespace MovieManager
{
	void start();
}
#endif
