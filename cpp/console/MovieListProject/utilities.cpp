#include"types.h"
#include"utilities.h"

// sorting
/*
	ssort:
	n - number of elements
	base - elements
	cmp - comparison function
	sz - element size
*/
void Utilities::ssort(void* base, size_t n, size_t sz, CFT cmp)
{
	// take the elements gap distance apart and sort them from right to left
	for(int gap=n/2; gap>0; gap/=2)
	{
		for(int i=gap; i<n; i++) // i means current end point; it increaments till size of the elements
		{
			// take elements gap distance apart from right to left
			for(int j=i-gap; j>=0; j-=gap)
			{
				// do necessary conversions
				char* b= static_cast<char*>(base);
				char* pj = b+j*sz; // &base[j]
				char* pjg = b+(j+gap)*sz; // &base[j+gap]

				// do comparison
				if(cmp(pj, pjg) < 0) // smaller
				{
					// swap
					for(int k=0; k<sz; k++)
					{
						char temp = pj[k];
						pj[k] = pjg[k];
						pjg[k] = temp;
					}
				}
			}
		}
	}
}

// comparison function for comparing string
int Utilities::cmpByString(const void* p, const void* q)
{
	//return strcmp(static_cast<const MovieInfo*>(p)->getName(), static_cast<const MovieInfo*>(q)->getName());
	return 0;
}

// comparison function for comparing ints
int Utilities::cmpByInt(const void* p, const void* q)
{
	return static_cast<const MovieInfo*>(q)->getType() - static_cast<const MovieInfo*>(p)->getType();
}

string Utilities::checkEmptyString(string name)
{
	return (name=="") ? " " : name;
}


Date Utilities::convertToDate(const string name)
{
	Logger::log("Entered Utilities::convertToDate() - "+name);
	int d,m,y;
		char temp[1024];
		strcpy(temp, name.c_str()); // copy line to temp
		char *token = strtok(temp, SEP_COMMA.c_str());
	if(token)
	d = atoi(token); // date

	Logger::log("first strtok");
	token = strtok(NULL, SEP_COMMA.c_str()); // month
	Logger::log("second strtok");
	if(token)
	m = atoi(token); // month

	token = strtok(NULL, SEP_COMMA.c_str()); // year
	Logger::log("third strtok");
	if(token)
	y = atoi(token);
	Date date(d, Date::Month(m), y);
	Logger::log("Exiting Utilities::convertToDate()");
	return date;
}
