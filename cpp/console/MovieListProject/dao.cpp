#include"types.h"
#include"dao.h"

string fileName = "movie_file_list.txt";
string flushy;
Vec<MovieInfo> gMovielist;
string SEP_BAR = "|";
string SEP_COMMA = ",";

/*
A sequence of two or more contiguous delimiter characters in the parsed string is considered to be a single delimiter.
*/
void Repository::readAll()
{
	Logger::debug("entered Repository::readAll");
	// read data from file
	string line;
	ifstream ifs(fileName.c_str());

	while(getline(ifs, line))
	{
		// tokenize string now
		char temp[1024];
		strcpy(temp, line.c_str());
		char *token = strtok(temp, "|");
		// create Movie record
		Date d;
		MovieInfo movieInfo(d);
		Logger::info("initialized movie record");
		if(token)
		movieInfo.setType(atoi(token));
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setName(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setDirectors(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setWriters(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setMusicDirectors(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setCinematographers(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setEditors(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setCasts(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setProductionCompanyNames(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setDistributors(token);
		token = strtok(NULL, "|");
		if(token)
		movieInfo.setReleaseDate(Utilities::convertToDate(token));
		Logger::debug(token);
		token = strtok(NULL, "|");
		if(token)
		{
		Logger::debug(token);
		movieInfo.setWatchDate(token);
		}

		gMovielist.push_back(movieInfo);
	}
	ifs.close();
	Logger::debug("exiting Repository::readAll");
}

void Repository::create(MovieInfo record)
{
	Logger::info("entered Repository::create");
	Date d = record.getReleaseDate();
	Logger::debug("r date: "+std::to_string(d.day())
	+std::to_string(d.month())
	+std::to_string(d.year())
	+d.stringRep());
	ofstream ofs(fileName.c_str(), ios::out | ios::app); // take output stream of file - to output data TO the file
	// create output stream iterator for outputing data to the file
	ostream_iterator<string> oStreamIterator(ofs, "\n");

	// push data to file
	*oStreamIterator = createRecordString(record);// movie info
	Logger::info("exiting Repository::create");
}

void Repository::update(MovieInfo movieInfo)
{
	// do changes in file now
	string line;
	ifstream ifs(fileName.c_str()); // get file input stream

	string tempFN = "temp.txt";
	ofstream ofs(tempFN.c_str()); // get file output stream
	cout << "Updating movie " << movieInfo.getName() << endl;
	while(getline(ifs, line))
	{
		char temp[1024];
		strcpy(temp, line.c_str()); // copy line to temp
		char *token = strtok(temp, SEP_BAR.c_str());
		token = strtok(NULL, "|"); // take movie name

		// write all lines other than the movie name to be deleted
		if(token != movieInfo.getName())
		{
			ofs << line<< endl;	
		}else
		{
			ofs << createRecordString(movieInfo) << endl;	
		}
	}
	

	ifs.close();
	ofs.close();

	const char *p = fileName.c_str();
	remove(p);
	rename(tempFN.c_str(), p);

}

string Repository::createRecordString(MovieInfo record)
{
string str = std::to_string(record.getType()) + SEP_BAR 
		+ record.getName() + SEP_BAR 
		+ record.getDirectors() + SEP_BAR 
		+ record.getWriters() + SEP_BAR 
		+ record.getMusicDirectors() + SEP_BAR 
		+ record.getCinematographers() + SEP_BAR
	 	+ record.getEditors() + SEP_BAR 
	 	+ record.getCasts() + SEP_BAR 
		+ record.getProductionCompanyName() + SEP_BAR 
		+ record.getDistributors() + SEP_BAR 
		+ record.getReleaseDate().stringRep() + SEP_BAR 
		+ record.getWatchDate(); 

	 return str;
}


void Repository::deleteRecord(string movieNameToDelete)
{
	string line;
	ifstream ifs(fileName.c_str()); // get file input stream

	string tempFN = "temp.txt";
	ofstream ofs(tempFN.c_str()); // get file output stream
	cout << "Removing movie " << movieNameToDelete << endl;
	while(getline(ifs, line))
	{
		char temp[1024];
		strcpy(temp, line.c_str()); // copy line to temp
		char *token = strtok(temp, SEP_BAR.c_str());
		token = strtok(NULL, "|"); // take movie name

		// write all lines other than the movie name to be deleted
		if(token != movieNameToDelete)
		{
			ofs << line << endl;	
		}
	}
	

	ifs.close();
	ofs.close();

	const char *p = fileName.c_str();
	remove(p);
	rename(tempFN.c_str(), p);
}
