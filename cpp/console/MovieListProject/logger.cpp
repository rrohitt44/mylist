#include"types.h"
#include"logger.h"

::FILE *fpLogfile;	
Logger::LOGGING_MODE mode;


void Logger::init()
{
	fpLogfile = fopen("log.txt","w"); // open file
	//mode=LOGGING_MODE::CONSOLE; // assign logging mode
	mode=LOGGING_MODE::FILE; // assign logging mode
	Logger::log("Logger Initialized");
}

void Logger::close()
{
	Logger::log("Exitting Logger");
	fclose(fpLogfile); // close the file
	delete fpLogfile; // free the memory occupied by file
}

void Logger::log(string s)
{
	if(mode==LOGGING_MODE::FILE)
	{
		fprintf(fpLogfile, "debug: %s\n", s.c_str());
	}
	else
	{
		cout << "debug: " << s << endl;
	}
}

void Logger::debug(string s)
{
	if(mode==LOGGING_MODE::FILE)
	{
		fprintf(fpLogfile, "debug: %s\n", s.c_str());
	}
	else
	{
		cout << "debug: " << s << endl;
	}
}

void Logger::info(string s)
{
	if(mode==LOGGING_MODE::FILE)
	{
		fprintf(fpLogfile, "info: %s\n", s.c_str());
	}
	else
	{
		cout << "info: " << s << endl;
	}
}

void Logger::error(string s)
{
	if(mode==LOGGING_MODE::FILE)
	{
		fprintf(fpLogfile, "error: %s\n", s.c_str());
	}
	else
	{
		cerr << "error: " << s << endl;
	}
}

void Logger::exception(string s)
{
	if(mode==LOGGING_MODE::FILE)
	{
		fprintf(fpLogfile, "exception: %s\n", s.c_str());
	}
	else
	{
		cout << "exception: " << s << endl;
	}
}
