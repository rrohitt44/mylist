#include"types.h"
#include"display.h"

// function prototype declarations
static void showMainMenu();
static void showMovieTypeMenu();
static void showMovieFields();

void Display::view(MovieInfo::MENU_TYPE type)
{
	switch(type)
	{
	case MovieInfo::MAIN_MENU: // main menu
	{
		showMainMenu();
	}
	break;
	case MovieInfo::MOVIE_TYPE_MENU: // movie type menu
	{
		showMovieTypeMenu();	
	}
	break;
	case MovieInfo::MOVIE_FIELD_MENU: // shows movie fields
	{
		showMovieFields();
	}
	break;
	}
	cout << "choose an option " << endl;

}

static void showMainMenu()
{
	cout << "1. Enter Movie Details" << endl;
	cout << "2. View Movie Details"<< endl;
	cout << "3. View All Movie Details" << endl;
	cout << "4. Remove Movie" << endl;
	cout << "5. Update Movie" << endl;
	cout << "6. Sort By Type" << endl;
	cout << "0. Exit" << endl;
}

static void showMovieFields()
{
	cout << "1. Movie Type" << endl;
	cout << "2. Movie Name" << endl;
	cout << "3. Director" << endl;
	cout << "4. Written By" << endl;
	cout << "5. Edited By" << endl;
	cout << "6. Cinematography By" << endl;
	cout << "7. Edited By" << endl;
	cout << "8. Starring" << endl;
	cout << "9. Production Company" << endl;
	cout << "10. Distributed By" << endl;
	cout << "11. Release Date" << endl;
	cout << "12. Watch Date" << endl;

}

static void showMovieTypeMenu()
{
	cout << "1. HOLLYWOOD" << endl;
	cout << "2. BOLLYWOOD" << endl;
	cout << "3. MARATHI" << endl;
}

MovieInfo Display::getMovieRecord()
{
	// local variables
	Date defDate;
	MovieInfo movieInfo(defDate);
	string temp;

	// flush buffer
	
	// create movie entry
	Display::view(MovieInfo::MENU_TYPE::MOVIE_TYPE_MENU); // shows movie type menu

	// enter movie details
	int iMovieType;
	cin >> iMovieType;
	movieInfo.setType(iMovieType);

	cout << "Movie Name:"<<endl;
	//cin >> movieInfo.name;
	//cin >> flushy;
	getline(cin,flushy); //movieInfo.name);
	getline(cin, temp);
	movieInfo.setName(Utilities::checkEmptyString(temp));
	
	cout << "Director:"<<endl;
	getline(cin, temp);
	movieInfo.setDirectors(Utilities::checkEmptyString(temp));
	
	cout << "Written by: " << endl;
	getline(cin, temp);
	movieInfo.setWriters(Utilities::checkEmptyString(temp));

	cout << "Music by: " << endl;
	getline(cin, temp);
	movieInfo.setMusicDirectors(Utilities::checkEmptyString(temp));

	cout << "Cinematography by: " << endl;
	getline(cin, temp);
	movieInfo.setCinematographers(Utilities::checkEmptyString(temp));

	cout << "Edited by: " << endl;
	getline(cin, temp);
	movieInfo.setEditors(Utilities::checkEmptyString(temp));

	cout << "Starring: " << endl;
	getline(cin, temp);
	movieInfo.setCasts(Utilities::checkEmptyString(temp));

	cout << "Production company: " << endl;
	getline(cin, temp);
	movieInfo.setProductionCompanyNames(Utilities::checkEmptyString(temp));

	cout << "Distributed by: " << endl;
	getline(cin, temp);
	movieInfo.setDistributors(Utilities::checkEmptyString(temp));

	cout << "Release date: " << endl;
	//getline(cin, temp);
	Date date = Display::acceptDate();
	movieInfo.setReleaseDate(date);

	cout << "Watch Date: "<<endl;
	getline(cin, flushy);
	getline(cin, temp);
	movieInfo.setWatchDate(Utilities::checkEmptyString(temp));

	// add movie to global movie list
	gMovielist.push_back(movieInfo);	
	return movieInfo;

}
void Display::printMovieEntry(int i)
{
	cout << "----------------------------------------" << endl;
	cout << "Movie Type: "<<getMovieType(gMovielist[i].getType())<<endl;
	cout << "Movie Name: "<<gMovielist[i].getName()<< endl;
	cout << "Director: " << gMovielist[i].getDirectors() << endl;
	cout << "Written by: " << gMovielist[i].getWriters() << endl;
	cout << "Cinematography by: " << gMovielist[i].getCinematographers() << endl;
	cout << "Edited by: " << gMovielist[i].getEditors() << endl;
	cout << "Starring: " << gMovielist[i].getCasts() << endl;
	cout << "Production company: " << gMovielist[i].getProductionCompanyName() << endl;
	cout << "Distributed by: " << gMovielist[i].getDistributors() << endl;
	cout << "Release date: " << gMovielist[i].getReleaseDate().stringRep() << endl;
	cout << "Watch Date: " << gMovielist[i].getWatchDate() << endl;
	cout << "----------------------------------------" << endl;
}

void Display::printMovieEntryByObject(MovieInfo movieInfo)
{
	cout << "----------------------------------------" << endl;
	cout << "Movie Type: "<<getMovieType(movieInfo.getType()) << endl;
	cout << "Movie Name: "<<movieInfo.getName()<< endl;
	cout << "Director: " << movieInfo.getDirectors() << endl;
	cout << "Written by: " << movieInfo.getWriters() << endl;
	cout << "Cinematography by: " << movieInfo.getCinematographers() << endl;
	cout << "Edited by: " << movieInfo.getEditors() << endl;
	cout << "Starring: " << movieInfo.getCasts() << endl;
	cout << "Production company: " << movieInfo.getProductionCompanyName() << endl;
	cout << "Distributed by: " << movieInfo.getDistributors() << endl;
	cout << "Release date: " << movieInfo.getReleaseDate().stringRep() << endl;
	cout << "Watch Date: " << movieInfo.getWatchDate() << endl;
	cout << "----------------------------------------" << endl;
}

void Display::printAllMovies()
{
	/*for(int i=0; i<gMovielist.size(); i++)
	{
		printMovieEntry(i);
	}*/

	// using iterator
	vector<MovieInfo>::const_iterator it;
	for(it = gMovielist.begin(); it != gMovielist.end(); it++)
	{
		const MovieInfo& info = *it;
		printMovieEntryByObject(info);
	}
}

int Display::showMovieListAndChooseTheOne()
{
	int input;
	
	// display movie list
	showMovieList();

	cout << "choose any one from above" << endl;
	cin >> input;
	return input;
}

void Display::showMovieList()
{
	for(int i=0; i<gMovielist.size(); i++)
	{
		cout << i << ". " << gMovielist[i].getName() << endl;
	}
}

string Display::chooseMovieName()
{
	int index = showMovieListAndChooseTheOne(); // get the index of the movie
	string mn =  gMovielist[index].getName();
	gMovielist.erase(gMovielist.begin()+index); // remove movie name from memory
	return mn;
}

string Display::getMovieType(int pMovieType)
{
	MovieInfo::MOVIE_TYPE movieType;
	string res;
	switch(pMovieType)
	{
		case 1:
			movieType = MovieInfo::HOLLYWOOD;
			res = "HOLLYWOOD";
		break;

		case 2:
			movieType = MovieInfo::BOLLYWOOD;
			res = "BOLLYWOOD";
		break;

		case 3:
			movieType = MovieInfo::MARATHI;
			res = "MARATHI";
		break;
	}
	return res;
}

Date Display::acceptDate()
{
	int d,m,y;
	cout << "Enter Day:"<<endl;
	cin >> d;
	cout << "Enter Month:"<<endl;
	cin >> m;
	cout << "Enter Year:"<<endl;
	cin >> y;
	Date date(d,Date::Month(m),y);
	return date;
}
