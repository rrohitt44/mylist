#ifndef H_DISPLAY
#define H_DISPLAY

#include"types.h"
#include"utilities.h"

namespace Display
{
	void printMovieEntry(int i);
	void printMovieEntryByObject(MovieInfo movieInfo);
	void printAllMovies();
	int showMovieListAndChooseTheOne();
	void showMovieList();
	string chooseMovieName();
	string getMovieType(int movieType);
	MovieInfo getMovieRecord();
	void view(MovieInfo::MENU_TYPE type);
	Date acceptDate();
}
#endif
