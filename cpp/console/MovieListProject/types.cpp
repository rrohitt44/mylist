#include"types.h"


// Date constructor
Date::Date(int dd, Month mm, int yy)
{
	cacheValid = false;
	if(dd==0)  dd=defaultDate.day();
	if(mm==0) mm=defaultDate.month();
	if(yy==0) yy=defaultDate.year();

	// handle leap year
	int max;
	switch(mm)
	{
		case FEB:
			max=28+0; // leapYear(yy);
		break;
		case APRL:
		case JUN:
		case SEPT:
		case NOV:
			max=30;
		break;
		case JAN:
		case MAR:
		case MAY:
		case JUL:
		case AUG:
		case OCT:
		case DEC:
			max=31;
		break;
		default:
			throw BadDate();
	}

	if(dd<1 || max<dd)
		throw BadDate();

	y = yy;
	m = mm;
	d = dd;
}

/*
Date::Date(int dd, int mm, int yy)
{
	Date(dd,Month(mm),yy); 
}*/

// set default date
Date Date::defaultDate(16,Date::DEC,1770);
void Date::setDefault(int dd, Month mm, int yy)
{
	Date::defaultDate = Date(dd, mm, yy);
}

Date& Date::addDay(int n)
{
	d += n;
	return *this;
}

Date& Date::addMonth(int n)
{
	m += n;
	return *this;

}

Date& Date::addYear(int n)
{
	y += n;
	return *this;
}

// computeCacheValue: convert date to string
void Date::computeCacheValue() const
{
	cache=std::to_string(d)+"/"+std::to_string(m)+"/"+std::to_string(y);
}

// stringRep: string representation of Date
string Date::stringRep() const // states that the function is not modifying the data in the class
{
	Logger::debug("entered stringRep:"+cache+" "+to_string(cacheValid));
	if(cacheValid == false)
	{
		Logger::debug("cache is not valid. caching started");
		computeCacheValue();
		cacheValid= true;
	}

	Logger::debug("stringRep:"+cache+" "+std::to_string(cacheValid)+" "+
	std::to_string(d)+" "+std::to_string(m)+" "+std::to_string(y)+" "
	+cache);
	return cache;
}

int Date::day() const
{
	return d;
}

Date::Month Date::month() const
{
	return Month(m);
}

int Date::year() const
{
	return y;
}

MovieInfo::MovieInfo(Date d):release_date(d){}

int MovieInfo::getType() const
{
	return type;
}
string MovieInfo::getName() const
{
	return name;
}
string MovieInfo::getDirectors() const
{
	return directed_by;
}
string MovieInfo::getWriters() const
{
	return written_by;
}
string MovieInfo::getMusicDirectors() const
{
	return music_by;
}
string MovieInfo::getCinematographers() const
{
	return cinematography_by;
}
string MovieInfo::getEditors() const
{
	return edited_by;
}
string MovieInfo::getCasts() const
{
	return starring;
}
string MovieInfo::getProductionCompanyName() const
{
	return production_company_name;
}
string MovieInfo::getDistributors() const
{
	return directed_by;
}
Date MovieInfo::getReleaseDate() const
{
	return this->release_date;
}
string MovieInfo::getWatchDate() const
{
	return watch_date;
}

MovieInfo& MovieInfo::setType(int type)
{
	this->type = type;
	return *this;
}
MovieInfo& MovieInfo::setName(string name)
{
	this->name = name;
	return *this;
}
MovieInfo& MovieInfo::setDirectors(string name)
{
	this->directed_by = name;
	return *this;
}
MovieInfo& MovieInfo::setWriters(string name)
{
	this->written_by = name;
	return *this;
}
MovieInfo& MovieInfo::setMusicDirectors(string name)
{
	this->music_by = name;
	return *this;
}
MovieInfo& MovieInfo::setCinematographers(string name)
{
	this->cinematography_by = name;
	return *this;
}
MovieInfo& MovieInfo::setEditors(string name)
{
	this->edited_by = name;
	return *this;
}
MovieInfo& MovieInfo::setCasts(string name)
{
	this->starring = name;
	return *this;
}
MovieInfo& MovieInfo::setProductionCompanyNames(string name)
{
	this->production_company_name = name;
	return *this;
}
MovieInfo& MovieInfo::setDistributors(string name)
{
	this->distributed_by = name;
	return *this;
}
MovieInfo& MovieInfo::setReleaseDate(Date name)
{
	this->release_date = name;
	return *this;
}
MovieInfo& MovieInfo::setWatchDate(string name)
{
	this->watch_date = name;
	return *this;
}


