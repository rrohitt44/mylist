#ifndef H_UTILITIES
#define H_UTILITIES

namespace Utilities
{
	typedef int (*CFT) (const void*, const void*);
	void ssort(void* base, size_t n, size_t sz, CFT cmp);
	// comparison functions
	int cmpByString(const void* p, const void* q);
	int cmpByInt(const void* p, const void* q);
	
	//extern Vec<MovieInfo> gMovielist;
	//using namespace std;
	string checkEmptyString(string name);
	Date convertToDate(string name);
}

#endif
