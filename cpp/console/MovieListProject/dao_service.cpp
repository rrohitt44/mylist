#include"dao_service_interface.h"

void DAOServiceInterface::loadRecords()
{
	// load exiting movie records globally
	Logger::debug("Entered DAOServiceInterface::loadRecords()");
	Repository::readAll();
	Logger::debug("Exiting DAOServiceInterface::loadRecords()");
}

int DAOServiceInterface::addRecord(MovieInfo movieInfo)
{
	Logger::info("entered DAOServiceInterface::addRecord");
	Repository::create(movieInfo);
	Logger::info("exiting DAOServiceInterface::addRecord");
	return 0;
}

int DAOServiceInterface::deleteRecord(string movieName)
{
	// delete the selected movie name
	Repository::deleteRecord(movieName);
	return 0;			
}

int DAOServiceInterface::updateRecord(int index)
{
	// local variables
	string temp;
	MovieInfo movieInfo = gMovielist[index];
	int inputMovieUpdate;
	MenuServiceInterface::view(MovieInfo::MENU_TYPE::MOVIE_FIELD_MENU); // show movie fields
	cin >> inputMovieUpdate;

	string flushy;
	cout << "Enter Details Now " << endl;
	getline(cin,flushy); //movieInfo.name);

	switch(inputMovieUpdate)
	{
		case 1: // type
			MenuServiceInterface::view(MovieInfo::MENU_TYPE::MOVIE_TYPE_MENU); // shows movie type menu
			// enter movie details
			int iMovieType;
			cin >> iMovieType;
			movieInfo.setType(iMovieType);
		break;

		case 2: // name
			getline(cin, temp);
			movieInfo.setName(temp);
		break;

		case 3: // director
		getline(cin, temp);
		movieInfo.setDirectors(temp);
		break;

		case 4: // written by
		getline(cin, temp);
		movieInfo.setWriters(temp);
		break;

		case 5: // music by
		getline(cin, temp);
		movieInfo.setMusicDirectors(temp);
		break;

		case 6: // cinematography
		getline(cin, temp);
		movieInfo.setCinematographers(temp);
		break;

		case 7: // edited by
		getline(cin, temp);
		movieInfo.setEditors(temp);
		break;

		case 8: // starring
		getline(cin, temp);
		movieInfo.setCasts(temp);
		break;

		case 9: // production company
		getline(cin, temp);
		movieInfo.setProductionCompanyNames(temp);
		break;

		case 10: // distributed by
		getline(cin, temp);
		movieInfo.setDistributors(temp);
		break;

		case 11: // release date
		//getline(cin, temp);
		movieInfo.setReleaseDate(Display::acceptDate());
		break;

		case 12: // watch date
		getline(cin, temp);
		movieInfo.setWatchDate(temp);
		break;
		
		default:
		cout << "Not considering any option for movie update" << endl;
	}

	// update in memory copy
	gMovielist[index] = movieInfo;


	// update the selected movie name
	Repository::update(movieInfo);
	return 0;					
}
