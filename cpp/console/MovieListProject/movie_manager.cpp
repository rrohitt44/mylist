#include"movie_manager.h"

void MovieManager::start()
{
	// start the logger
	Logger::init();

	Logger::log("Entered MovieManager::start()");
	int option;
	string movieName; // for movie name to be deleted from file
	int movieIndex=0; // movie index in list
	Date date;
	MovieInfo record(date);

	Logger::log("Before Load Records");
	DAOServiceInterface::loadRecords(); // load records globally
	Logger::log("After Load Records");
	do
	{
		MenuServiceInterface::view(MovieInfo::MENU_TYPE::MAIN_MENU); // main menu
		cin >> option;

		// filter selected option now
		switch(option)
		{
			case 1: // add movie
				record = MenuServiceInterface::getMovieRecord();
				DAOServiceInterface::addRecord(record);
			break;

			case 2: // view specific movie details
				movieIndex = MenuServiceInterface::showMovieListAndChooseTheOne();
				try
				{
					MenuServiceInterface::viewMovieDetails(movieIndex);
				}catch(out_of_range)
				{
					cerr<<"Range error"<<endl;
					cerr<<"No movie with index " << movieIndex << " found."<<endl;
				}
			break;

			case 3: // show all movie details
				MenuServiceInterface::showAllMovieDetails();
			break;
			
			case 4: // remove movie
				try
				{
					movieName = MenuServiceInterface::chooseMovieName();
					// delete the selected movie name
					DAOServiceInterface::deleteRecord(movieName);
					cout << "Successfully deleted movie with name " << movieName << endl;
			
				}catch(out_of_range)
				{
					cerr << "Range error" << endl;
					cerr << "No movie with name " << movieName << " found." << endl;
				}

				break;
			case 5: // update movie
				try
				{
					movieIndex = MenuServiceInterface::showMovieListAndChooseTheOne();
					DAOServiceInterface::updateRecord(movieIndex);
					cout << "Successfully updated movie with name " << movieName << endl;
			
				}catch(out_of_range)
				{
					cerr << "Range error" << endl;
					cerr << "No movie with name " << movieName << " found." << endl;
				}

				break;

			case 6: // sort by type
				Utilities::ssort(&gMovielist, gMovielist.size(), sizeof(MovieInfo), Utilities::cmpByInt);
				MenuServiceInterface::showMovieList();
				break;

			default:
				if(option != 0)
				{
					Logger::log("MAINSWITCH::choose proper option");
				}
			break;
		}
		cout << endl << endl;
	}while(option!=0);

	// close logger
	Logger::close();
}
